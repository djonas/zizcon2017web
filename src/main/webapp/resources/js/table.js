var cntrlIsPressed = false;
var locale = 'cs';

function renderTimetable() {
  var type= $('#type').val();
  var myActivities= $('#myActivities').is(":checked");
  $.getJSON("/api/getSchedule?type="+type+"&myActivities="+myActivities, function( data ) {
  var groups = new vis.DataSet();
  if(locale == 'cs'){
        groups.add({id: 17, content: 'Neděle 17.'});
      groups.add({id: 16, content:  'Sobota 16.'});
      groups.add({id: 15, content: 'Pátek 15.'});
      groups.add({id: 14, content: 'Čtvrtek 14.'});
  } else {
    groups.add({id: 17, content: 'Sunday 17.'});
    groups.add({id: 16, content:  'Saturday 16.'});
    groups.add({id: 15, content: 'Friday 15.'});
    groups.add({id: 14, content: 'Thursday 14.'});
  }
    // create a dataset with items
    var items = new vis.DataSet();
    for (var i = 0; i < data.length; i++) {
      var activity = data[i];
      var cssClass = (activity.attendable)? "attendable "+activity.type : "not-attendable "+activity.type;
      if(activity.reasonNotAttendable == 'activity.notAttendable.alreadyAttending'){
        cssClass = cssClass + ' attending'
      }
      items.add({
        id: i,
        className: cssClass,
        group: resolveDay(activity.from),
        content: (activity.maxAttendees > 0? activity.title + "&nbsp;("+ activity.attendees + "/" + activity.maxAttendees + ")" : activity.title) ,
        start: setSameDay(activity.from),
        end: setSameDay(activity.to),
        url : '/program/detail/'+activity.stringId
      });
    }

    // create visualization
    var container = document.getElementById('visualization');
    var options = {
      groupOrder: 'id',
      zoomable: false,
      zoomMin:36000000,
      zoomMax:36000000,
      min:new Date(2017,8,14,8,0),
      max:new Date(2017,8,14,23,59),
      dataAttributes: ['url'],
      orientation: 'both',
      width: '100%',
      verticalScroll: true
    };

    var timeline = new vis.Timeline(container);
    timeline.setOptions(options);
    timeline.setGroups(groups);
    timeline.setItems(items);
    $('#visualization').click(function(event){
      var props = timeline.getEventProperties(event);
      if(items.get(props.item).url && "" != items.get(props.item).url){
        if(cntrlIsPressed){
          window.open(items.get(props.item).url, '');
        }else {
          window.location.href=items.get(props.item).url;
        }

      }
    });
  });
}

$(document).ready( function(){
  renderTimetable();
});
$(document).ready( function(){
  locale = $('#visualization').attr('data-locale');
});

$('#program-controls').change(function(event){
  renderTimetable();
});

$(document).keydown(function(event){
  if(event.which=="17")
    cntrlIsPressed = true;
});

$(document).keyup(function(){
  cntrlIsPressed = false;
});




function resolveDay(from) {
  var date = new Date(from);
  return date.getUTCDate();
}

function setSameDay(date){
  var date = new Date(date);
  date.setUTCDate(14);
  return date;
}
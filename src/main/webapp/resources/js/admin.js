function createOptions(stepfunction) {
  return {
    color: '#aaa',
    // This has to be the same size as the maximum width to
    // prevent clipping
    strokeWidth: 10,
    trailWidth: 10,
    easing: 'easeInOut',
    duration: 1400,
    text: {},
    from: {color: '#333', width: 10},
    to: {color: '#333', width: 10},
    // Set default step function for all animate calls
    step: stepfunction
  };
}

var numOfRegisteredBar = new ProgressBar.Circle('#numOfRegistered',
    createOptions(
        function (state, circle) {
          circle.path.setAttribute('stroke', state.color);
          circle.path.setAttribute('stroke-width', state.width);

          var value = Math.round(circle.value() * 500);
          if (value === 0) {
            circle.setText('0');
          } else {
            circle.setText(value);
          }
        }));

var numOfRegistered = $('#numOfRegistered').attr('data-value');
var numOfRegisteredValue = numOfRegistered / 500;
numOfRegisteredBar.animate(numOfRegisteredValue);  // Number from 0.0 to 1.0

var numOfPaidRegistrationsBar = new ProgressBar.Circle('#numOfPaidRegistrations',
    createOptions(
        function (state, circle) {
          circle.path.setAttribute('stroke', state.color);
          circle.path.setAttribute('stroke-width', state.width);

          var value = Math.round(circle.value() * numOfRegistered);
          if (value === 0) {
            circle.setText('0');
          } else {
            circle.setText(value);
          }
        }));

var numOfPaidRegistrations = $('#numOfPaidRegistrations').attr('data-value');
var numOfPaidRegistrationsValue = numOfPaidRegistrations / numOfRegistered;
numOfPaidRegistrationsBar.animate(numOfPaidRegistrationsValue);  // Number from 0.0 to 1.0


var numOfUnpaidRegistrationsBar = new ProgressBar.Circle('#numOfUnpaidRegistrations',
    createOptions(
        function (state, circle) {
          circle.path.setAttribute('stroke', state.color);
          circle.path.setAttribute('stroke-width', state.width);

          var value = Math.round(circle.value() * numOfRegistered);
          if (value === 0) {
            circle.setText('0');
          } else {
            circle.setText(value);
          }
        }));

var numOfUnpaidRegistrations = $('#numOfUnpaidRegistrations').attr('data-value');
var numOfUnpaidRegistrationsValue = 1- numOfPaidRegistrations / numOfRegistered;
numOfUnpaidRegistrationsBar.animate(numOfUnpaidRegistrationsValue);  // Number from 0.0 to 1.0


var numOfActivitiesBar = new ProgressBar.Circle('#numOfActivities',
    createOptions(
        function (state, circle) {
          circle.path.setAttribute('stroke', state.color);
          circle.path.setAttribute('stroke-width', state.width);

          var value = Math.round(circle.value() * 150);
          if (value === 0) {
            circle.setText('0');
          } else {
            circle.setText(value);
          }
        }));

var numOfActivities = $('#numOfActivities').attr('data-value');
var numOfActivitiesValue = numOfActivities/150;
numOfActivitiesBar.animate(numOfActivitiesValue);  // Number from 0.0 to 1.0

var numOfFullActivitiesBar = new ProgressBar.Circle('#numOfFullActivities',
    createOptions(
        function (state, circle) {
          circle.path.setAttribute('stroke', state.color);
          circle.path.setAttribute('stroke-width', state.width);

          var value = Math.round(circle.value() * numOfActivities);
          if (value === 0) {
            circle.setText('0');
          } else {
            circle.setText(value);
          }
        }));

var numOfFullActivities = $('#numOfActivities').attr('data-value');
var numOfFullActivitiesValue = numOfFullActivities / numOfActivities;
numOfFullActivities.animate(numOfFullActivitiesValue);  // Number from 0.0 to 1.0
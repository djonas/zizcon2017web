<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<section class="subpage" id="anotherActivities">
    <div class="container">
        <div class="center section-text"><spring:message code="aboutus.anotherActivities.sectionText"/>
        </div>
        <div class="row">
            <div class="col-md-8 col-sm-12 ">
                <div class="row">
                    <h3 class="red">ŽIŽCONÍK</h3>
                    <p><spring:message code="aboutus.anotherActivities.zizconik.text"/></p>
                    <h3 class="red"><spring:message code="aboutus.anotherActivities.club.title"/></h3>
                    <p><spring:message code="aboutus.anotherActivities.club.text"/>
                    </p>
                    <ul>
                        <li><spring:message code="aboutus.anotherActivities.club.first"/></li>
                        <li><spring:message code="aboutus.anotherActivities.club.second"/></li>
                        <li><spring:message code="aboutus.anotherActivities.club.third"/></li>
                    </ul>
                    <h3 class="red"><spring:message code="aboutus.anotherActivities.events.title"/></h3>
                    <p><spring:message code="aboutus.anotherActivities.events.text"/></p>
                </div>
            </div>
            <div class="col-md-4 hidden-md-down"><img class="img-responsive" src="/resources/img/anotherActivities.jpg" alt="ŽižCon na Blaviconu"> </div>
        </div>
    </div>
</section>
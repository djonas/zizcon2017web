<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<section class="subpage" id="whoarewe">
    <div class="container">
        <div class="center section-text"><spring:message code="aboutus.whoarewe.sectionText"/>
        </div>
        <div class="row">
            <div class="col-md-8 col-sm-12 ">
                <div class="row">
                    <p><spring:message code="aboutus.whoarewe.text"/></p>
                    <h3 class="red"><spring:message code="aboutus.whoarewe.aboutZizcon.title"/></h3>
                    <p><spring:message code="aboutus.whoarewe.aboutZizcon.text"/></p>
                </div>
            </div>
            <div class="col-md-4 hidden-md-down"><img class="img-responsive" src="/resources/img/whoarewe.jpg" alt="Okrasný ředitel se Žižkou"> </div>
        </div>
    </div>
</section>
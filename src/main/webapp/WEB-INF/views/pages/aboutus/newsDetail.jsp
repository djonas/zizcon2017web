<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<section class="subpage" id="rules">
    <div class="container">
        <div class="center section-text">
            <c:out value="${article.summary}" escapeXml="false"/>
        </div>
        <div class="content">
            <c:out value="${article.fullTextation}" escapeXml="false"/>
        </div>
        <div class="article-information">
            <fmt:formatDate pattern="dd.MM.yyyy" value = "${article.published}" /> | <c:out value="${article.author}" escapeXml="false"/>
        </div>
    </div>
</section>
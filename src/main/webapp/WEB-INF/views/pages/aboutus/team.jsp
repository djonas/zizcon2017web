<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<section class="subpage" id="team">
    <div class="container">
        <div class="center section-text"><spring:message code="aboutus.team.sectionText"/>
        </div>
        <div class="content">
            <div class="row team-member">
                <div class="team-member-img col-lg-3 col-md-4 ">
                    <img class="img-responsive img-circle center" src="/resources/img/team/jirous.jpg" alt="Filip Jirouš v celé své kráse"/>
                </div>
                <div class="team-member-text col-lg-9 col-md-8">
                    <h3>Filip Jirouš</h3>
                    <small>Arcipředseda</small>
                    <p>Jsem vlastně jedna velká upomínka – neustále připomínám lidem, že něco chybí a že je něco potřeba dodělat. Taky jsem ten, co se musí tvářit, že tomu šéfuje a hlavně, že ví, co dělá. Ujíždím na knížkách, hudbě, filmech, Číně, horách a pivu. A taky podle toho vypadám.</p>
                </div>
            </div>
            <div class="row team-member">
                <div class="team-member-img col-lg-3 col-md-4 ">
                    <img class="img-responsive img-circle" src="/resources/img/team/denemark.jpg" alt="Ivo Denemark v celé své kráse"/>
                </div>
                <div class="team-member-text col-lg-9 col-md-8">
                    <h3>Ivo Denemark</h3>
                    <small>Okrasný ředitel festivalu</small>
                    <p>Mým úkolem je zajišťovat nadbytečné věci, bez kterých by se festival zcela jistě obešel, jako jsou prostory, návštěvníci a finance. Nejradši si zahraji originální RPG a pořádně epické deskovky s hutným příběhem.</p>
                </div>


            </div>
            <div class="row team-member">
                <div class="team-member-img col-lg-3 col-md-4 ">
                    <img class="img-responsive img-circle" src="/resources/img/team/stojan.jpg" alt="Jakub Stojan v celé své kráse"/>
                </div>
                <div class="team-member-text col-lg-9 col-md-8">
                    <h3>Jakub Stojan</h3>
                    <small>Šéf programu</small>
                    <p>Mám na starosti fungování programu, tedy aby každý kdo má zájem něco hrát našel na našem programu to co hledá, Jsem tedy vlastně takový Tinder ŽižConu. Svému okolí zatím úspěšně tajím svoje nerdovství jízdou na motorce a péčí o svůj zevnějšek.</p>
                </div>
            </div>
            <div class="row team-member">
                <div class="team-member-img col-lg-3 col-md-4 ">
                    <img class="img-responsive img-circle" src="/resources/img/team/valtr.jpg" alt="Václav Valtr v celé své kráse"/>
                </div>
                <div class="team-member-text col-lg-9 col-md-8">
                    <h3>Václav Valtr</h3>
                    <small>Ředitel LARP sekce</small>
                    <p>Nadšenec do příběhů a jiných světů, ať už ve formě knih, divadla, filmů, rpg, vyprávění, anebo právě LARPů, o které se starám na Žižconu. Miluju deskovky, kartičky, košaté fikční vesmíry, halucinace, čaj, hudbu a tabák a nesnáším organizování. </p>
                </div>


            </div>
            <div class="row team-member">
                <div class="team-member-img col-lg-3 col-md-4 ">
                    <img class="img-responsive img-circle" src="/resources/img/team/denemarkova.jpg" alt="Martina Denemarková v celé své kráse"/>
                </div>
                <div class="team-member-text col-lg-9 col-md-8">
                    <h3>Martina Denemarková</h3>
                    <small>Ředitelka registrace</small>
                    <p>Jsem vlastně šedou eminencí Žižconu, a jako takovou mě ani koutkem oka nezahlédnete. K mým specialitám patří plížení se v datech korporace. Sice za účast nemůžete zaplatit ani Titcoinem ani Bitcoinem, ale určitě si vás všimnu.</p>
                </div>
            </div>
            <div class="row team-member">
                <div class="team-member-img col-lg-3 col-md-4 ">
                    <img class="img-responsive img-circle" src="/resources/img/team/sana.jpg" alt="Vojtěch Šána v celé své kráse"/>
                </div>
                <div class="team-member-text col-lg-9 col-md-8">
                    <h3>Vojtěch Šána</h3>
                    <small>Ředitel RPG sekce</small>
                    <p>Víte co je to Dračí doupě? Tak i tak můžou vypadat RPG hry, o které se na ŽiŽconu starám. Vytvářet ty nejšílenější světy na základě náhodných asociací, nebo taky knih, filmů, ale třeba i snů, mě bavilo už od mládí. Ředitel RPG sekce pro mě tedy byla jasná volba. Rád hraju rpgčka s jasnými pravidly a příběhem na pokračování, ať už fantasy, či sci-fi. Z deskovek mám rád jakékoliv epické hry, u kterých člověk stráví i celý den.</p>
                </div>


            </div>
            <div class="row team-member">
                <div class="team-member-img col-lg-3 col-md-4 ">
                    <img class="img-responsive img-circle" src="/resources/img/team/muzik.jpg" alt="Tomáš Mužík v celé své kráse"/>
                </div>
                <div class="team-member-text col-lg-9 col-md-8">
                    <h3>Tomáš „Tomislav“ Mužík</h3>
                    <small>Šéf vedených deskovek</small>
                    <p>Jako malej jsme začínal na Dračáku jednapětce, Osadnících a Bpačku (tom starém Krnovském). Jako velkej hraju všechno, co mně přijde pod ruku. Ať už jsi pěkně hutné RPG s příběhem, či drsná strategická deskovka nebo snad pořádný atmosferický larpík tak věz, že neunikneš mému pozornému oku… Na Žižconu mám na starost organizované hraní deskových her, ale v podstatě jenom všem do všeho kecám.</p>
                </div>
            </div>
            <div class="row team-member">
                <div class="team-member-img col-lg-3 col-md-4 ">
                    <img class="img-responsive img-circle" src="/resources/img/team/dvorak.jpg" alt="Martin Dvořák v celé své kráse"/>
                </div>
                <div class="team-member-text col-lg-9 col-md-8">
                    <h3>Martin "Smaug" Dvořák </h3>
                    <small>Šéf vedených deskovek II</small>
                    <p>Nepočítačové hry jsou dlouhodobě mojí největší vášní, ať už ve formě RPG nebo deskovek, k jejichž hraní mám přeci jen příležitost podstatně častěji. Celkem přirozeně jsem se přitom začal zajímat i o organizační aspekt této aktivity, protože kdo si nesežene spoluhráče, nehraje :-). ŽižCon pro mě tedy byla jasná volba ;). Další zájmy: četba (převážně fantastika), historie, cestování, pěší turistika a treking.</p>
                </div>


            </div>
            <div class="row team-member">
                <div class="team-member-img col-lg-3 col-md-4 ">
                    <img class="img-responsive img-circle" src="/resources/img/team/cizek.jpg" alt="Martin Čížek v celé své kráse"/>
                </div>
                <div class="team-member-text col-lg-9 col-md-8">
                    <h3>Martin Čížek</h3>
                    <small>Ředitel HR</small>
                    <p>Mým úkolem je pro ŽižCon shánět dobrovolníky, kterým láskyplně říkáme Peoni. Mou tvář nikdo nezná, takže si dobrovolníci nemají kde stěžovat. V civilu jsem učitel na vysoké škole, což pomáhá při motivaci k dobrovolnictví!</p>
                </div>
            </div>
            <div class="row team-member">
                <div class="team-member-img col-lg-3 col-md-4 ">
                    <img class="img-responsive img-circle" src="/resources/img/team/fort.jpg" alt="Tadeáš Fořt v celé své kráse"/>
                </div>
                <div class="team-member-text col-lg-9 col-md-8">
                    <h3>Tadeáš Fořt</h3>
                    <small>Ředitel gastra</small>
                    <p>Již od mala mi maminka říkala, ať se raději držím dál od kuchyně, protože jsem na to moc šikovný, takže když se mě kamarádi ptali, co bych chtěl dělat pro ŽižCon, nebylo co řešit. Doufám, že vám bude chutnat, protože plný žaludek je základem spokojeného hráče ;)</p>
                </div>


            </div>
            <div class="row team-member">
                <div class="team-member-img col-lg-3 col-md-4 ">
                    <img class="img-responsive img-circle" src="/resources/img/team/bazikova.jpg" alt="Kateřina Baziková v celé své kráse"/>
                </div>
                <div class="team-member-text col-lg-9 col-md-8">
                    <h3>Kateřina Baziková</h3>
                    <small>Ředitelka recepce</small>
                    <p>Strážím vstupní bránu, vybírám vstupné a starám se, aby byli všichni správně opáskováni jako hrdí účastníci ŽižConu. Ráda se chodím ztrácet do knih a filmů, znám prý až příliš mnoho britských komiků a poslední dobou ulítávám na hudbě Cream, King Crimson a T. Rex. V hlavě mám pestrou sbírku zajímavých faktů, které mi jsou většinou k ničemu - víte, za jak dlouho se upeče pizza na Venuši? A vím, že nikdy nebudu tak cool jako David Bowie.</p>
                </div>
            </div>
            <div class="row team-member">
                <div class="team-member-img col-lg-3 col-md-4 ">
                    <img class="img-responsive img-circle" src="/resources/img/team/obdrzalek.jpg" alt="Jonáš Obdržálek v celé své kráse"/>
                </div>
                <div class="team-member-text col-lg-9 col-md-8">
                    <h3>Jonáš Obdržálek</h3>
                    <small>Ředitel IT R&D týmu o jednom člověku</small>
                    <p>Výtvor nelegálních pokusů na prasečí DNA. Ve dne spasitel všeho jídla, v noci maskovaný bojovník proti hubnoucím centrům. Taky rád hraje deskové hry a mluví o sobě ve třetí osobě. Tento web vystavěl vlastnímy prasečími nožkami.</p>
                </div>


            </div>
        </div>
        <div class="article-information">
        </div>
    </div>
</section>
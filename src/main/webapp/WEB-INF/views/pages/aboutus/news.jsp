<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<section class="subpage" id="news">
    <div class="container">
        <c:forEach items="${news}" var="newsItem">
            <div class="row newsitem">
                <c:choose>
                    <c:when test="${empty newsItem.fullTextation}">
                        <h3 class="red">${newsItem.title}</h3>
                    </c:when>
                    <c:otherwise>
                        <a href="/aboutus/news/detail/${newsItem.id}"><h3 class="red">${newsItem.title}</h3></a>
                    </c:otherwise>
                </c:choose>
                <p class="date-published"><fmt:formatDate pattern="dd.MM.yyyy" value = "${newsItem.published}" /> | ${newsItem.author}</p>
                <p>${newsItem.summary}</p>
                <c:if test="${not empty newsItem.fullTextation}">
                    <p><a href="/aboutus/news/detail/${newsItem.id}">Číst dále</a></p>
                </c:if>
            </div>
        </c:forEach>
        <jsp:include page="../../../tiles/template/pagination.jsp"/>
    </div>

</section>


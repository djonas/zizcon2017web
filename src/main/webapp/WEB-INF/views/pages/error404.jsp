<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<section class="subpage" id="error">
    <div class="container">
        <div class="row">
            <div class="center section-text"><span class="bolder-text">Našel si konec internetu, tak raději utíkej zpátky!</span>
                ${errorMsg}
            </div>
    </div>
</section>
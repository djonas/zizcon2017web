<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<section class="subpage" id="rules">
    <div class="container">
        <div class="center section-text"><spring:message code="rules.sectionText" />
        </div>
        <div class="row">
            <c:set value="/resources/download/souhlas_rodicu.docx" var="downloadUrl"/>
            <spring:message code="rules.textation"  arguments="${downloadUrl}" />
        </div>
    </div>
</section>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<section id="aboutus">
    <div class="container">
        <div class="center wow fadeInDown">
            <h2 class="center"><spring:message code="homepage.aboutus.title" /></h2>
        </div>
        <div class="center section-text wow fadeInUp"><spring:message code="homepage.aboutus.sectionText" /></div>
        <div class="row activities wow fadeInDown">
            <div class="col-lg-3 col-md-6 activity">
                <div><img src="/resources/img/icon_boardgame.png" alt="<spring:message code="homepage.aboutus.activity.boardgame.title" />"></div>
                <h3 class="red oneline"><spring:message code="homepage.aboutus.activity.boardgame.title" /></h3>
                <p class="equal"><spring:message code="homepage.aboutus.activity.boardgame.text" /></p>
                <a href="/program/list?type=BG"><div class="activity-program"><spring:message code="homepage.aboutus.activity.boardgame.button" /></div></a>
            </div>
            <div class="col-lg-3 col-md-6 activity">
                <div><img src="/resources/img/icon_rpg.png" alt="<spring:message code="homepage.aboutus.activity.rpg.title" />"></div>
                <h3 class="red oneline"><spring:message code="homepage.aboutus.activity.rpg.title" /></h3>
                <p class="equal"><spring:message code="homepage.aboutus.activity.rpg.text" /></p>
                <a href="/program/list?type=RPG"><div class="activity-program"><spring:message code="homepage.aboutus.activity.rpg.button" /></div></a>
            </div>
            <div class="col-lg-3 col-md-6 activity">
                <div><img src="/resources/img/icon_larp.png" alt="<spring:message code="homepage.aboutus.activity.larp.title" />"></div>
                <h3 class="red oneline"><spring:message code="homepage.aboutus.activity.larp.title" /></h3>
                <p class="equal"><spring:message code="homepage.aboutus.activity.larp.text" /></p>
                <a href="/program/list?type=LARP"><div class="activity-program"><spring:message code="homepage.aboutus.activity.larp.button" /></div></a>
            </div>
            <div class="col-lg-3 col-md-6 activity">
                <div><img src="/resources/img/icon_other.png" alt="<spring:message code="homepage.aboutus.activity.other.title" />"></div>
                <h3 class="red"><spring:message code="homepage.aboutus.activity.other.title" /></h3>
                <p class="equal"><spring:message code="homepage.aboutus.activity.other.text" /></p>
                <a href="/program/list?type=OTH"><div class="activity-program"><spring:message code="homepage.aboutus.activity.other.button" /></div></a>
            </div>

        </div>

    </div>

    </div>
</section>

<section id="where">
    <div class="container-fluid">
        <div class="center wow fadeInDown">
            <h2 class="center"><spring:message code="homepage.where.title" /></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-xs-12 address">
            <div class="centered wow fadeInDown">
            <h3 class="red"><spring:message code="homepage.where.addressTitle" /></h3>
            <p><spring:message code="homepage.where.address" /></p>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12" id="map"> </div>
    </div>

</section>


<section id="sponsors">
    <div class="container">
        <div class="center wow fadeInDown">
            <h2 class="center"><spring:message code="homepage.sponsors.title" /></h2>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <a href="http://www.mindok.cz/cz/uvodni-strana" target="_blank"><img class="sponsor" src="/resources/img/supporters/mindok.png" alt="Mindok"></a>
            </div>
            <div class="col-sm-3">
                <a href="http://www.albi.cz/uvod/" target="_blank"><img class="sponsor" src="/resources/img/supporters/albi.png" alt="Albi"></a>
            </div>
            <div class="col-sm-3">
                <a href="http://blackfire.cz/" target="_blank"><img class="sponsor" src="/resources/img/supporters/blackfire.png" alt="Blackfire"></a>
            </div>
            <div class="col-sm-3">
                <a href="http://www.komunitnikostel.cz/" target="_blank"><img class="sponsor" src="/resources/img/supporters/komunitni-centrum.png" alt="Komunitní kostel"></a>
            </div>
            <div class="col-sm-3">
                <a href="http://oldstars.cz/index.php/article/topic/17" target="_blank"><img class="sponsor" src="/resources/img/supporters/h2o.png" alt="Oldstars"></a>
            </div>
            <div class="col-sm-3">
                <a href="https://www.tomovyhry.cz/" target="_blank"><img class="sponsor" src="/resources/img/supporters/tomovy-hry.png" alt="Tomovy Hry"></a>
            </div>
            <div class="col-sm-3">
                <a href="http://gamecon.cz/" target="_blank"><img class="sponsor" src="/resources/img/supporters/gamecon.png" alt="Gamecon"></a>
            </div>
            <div class="col-sm-3">
                <a href="http://gwint.cz/" target="_blank"><img class="sponsor" src="/resources/img/supporters/gwint.png" alt="Gwint"></a>
            </div>
        </div>
        <div class="row">

            <div class="col-sm-3">
                <a href="http://www.lorisgames.cz/cs/" target="_blank"><img class="sponsor" src="/resources/img/supporters/loris-games.png" alt="Loris Games"></a>
            </div>
            <div class="col-sm-3">
                <a href="https://www.kickstarter.com/projects/1603344639/godforsaken-scavengers-survival-card-game" target="_blank"><img class="sponsor" src="/resources/img/supporters/godforsaken-scavengers.png" alt="Godforsaken Scavengers"></a>
            </div>
            <div class="col-sm-3">
                <a href="https://www.zatrolene-hry.cz/" target="_blank"><img class="sponsor" src="/resources/img/supporters/zatrolene-hry.png" alt="Zatrolené Hry"></a>
            </div>
            <div class="col-sm-3">
                <a href="http://criticalhit.cz/" target="_blank"><img class="sponsor" src="/resources/img/supporters/critical-hit.png" alt="Critical Hit"></a>
            </div>
            <div class="col-sm-3">
                <a href="http://www.geekarna.cz/" target="_blank"><img class="sponsor" src="/resources/img/supporters/geekarna.png" alt="Geekárna"></a>
            </div>
            <div class="col-sm-3">
                <a href="http://www.pevnost.cz/" target="_blank"><img class="sponsor" src="/resources/img/supporters/pevnost.png" alt="Pevnost"></a>
            </div>
            <div class="col-sm-3">
                <a href="http://hrajeme.cz/" target="_blank"><img class="sponsor" src="/resources/img/supporters/hrajeme.png" alt="Hrajeme"></a>
            </div>
            <div class="col-sm-3">
                <a href="https://hrajlarp.cz/" target="_blank"><img class="sponsor" src="/resources/img/supporters/hraj-larp.png" alt="Hraj LARP"></a>
            </div>
        </div>
    </div>
</section>

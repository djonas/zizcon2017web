<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="row">
    <div class="col-lg-12"><h2><spring:message code="profile.myActivities.title"/></h2></div>
    <div class="col-lg-12">
        <c:choose>
            <c:when test="${empty activities}">
                <c:set var="emptyText" value="homepage.aboutus.activity"/>
                <div class="center section-text">
                        <span class="bolder-text">
                            <spring:message code="program.list.notAttending_"/>
                        </span>
                </div>
            </c:when>
            <c:otherwise>
                <div class="table-responsive registrations-table">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Typ</th>
                            <th>Název</th>
                            <th>Den/od/do</th>
                            <th>Ovládání</th>
                        </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${activities}" var="activity">
                                <tr>
                                    <td><spring:message code="program.list.item.type.${activity.type}"/></td>
                                    <td>${activity.title}</td>
                                    <td><fmt:formatDate pattern="dd" value="${activity.from}"/>.&nbsp;<fmt:formatDate pattern="HH:mm" value="${activity.from}"/>-<fmt:formatDate pattern="HH:mm" value="${activity.to}"/></td>
                                    <td><a class="btn attend-button" data-url="/user/action/cancelAttendance?activityId=${activity.stringId}"><spring:message code="program.list.info.signout"/></a></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </c:otherwise>
        </c:choose>
    </div>
</div>
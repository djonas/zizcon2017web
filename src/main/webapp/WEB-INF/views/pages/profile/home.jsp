<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="row">
    <div class="col-lg-12"><h2><spring:message code="profile.home.title"/></h2></div>
</div>
<div class="row">
    <div class="col-lg-12">
        <h3><spring:message code="profile.home.basicInfo"/></h3>
        <div class="user-detail">
            <spring:message code="profile.home.attribute.username"/>: ${user.username}
        </div>
        <div class="user-detail">
            <spring:message code="profile.home.attribute.email"/>: ${user.email}
        </div>
        <div class="user-detail">
            <spring:message code="profile.home.attribute.firstName"/>: ${user.firstName}
        </div>
        <div class="user-detail">
            <spring:message code="profile.home.attribute.surname"/>: ${user.surname}
        </div>
    </div>
</div>
<div class="row divider">
    <div class="col-lg-12">
        <h3><spring:message code="profile.home.address"/></h3>
        <div class="user-detail">
            <spring:message code="profile.home.attribute.street"/>: ${user.street}
        </div>
        <div class="user-detail">
            <spring:message code="profile.home.attribute.city"/>: ${user.city}
        </div>
        <div class="user-detail">
            <spring:message code="profile.home.attribute.zipCode"/>: ${user.zipCode}
        </div>
    </div>
</div>
<div class="row divider">
    <div class="col-lg-12">
        <h3><spring:message code="profile.home.changePassword"/></h3>
        <form:form method="post" modelAttribute="user" action="/user/action/changePassword" accept-charset="UTF-8">
            <form:hidden path="id"/>
            <form:hidden path="username"/>
            <div class="">
                <spring:bind path="password">
                    <spring:message code="profile.form.password" var="password"/>
                    <div class="form-group required ${status.error ? 'has-error' : ''}">
                        <label class=" control-label">${password}</label>
                        <div class="">
                            <form:input path="password" type="password" class="form-control"
                                        id="password" placeholder=""/>
                            <form:errors path="password" class="control-label"/>
                        </div>
                    </div>
                </spring:bind>
            </div>
            <div class=" ">
                <spring:bind path="password">
                    <spring:message code="profile.form.passwordAgain" var="passwordAgain"/>
                    <div class="form-group required ${status.error ? 'has-error' : ''}">
                        <label class="control-label">${passwordAgain}</label>
                        <div class="">
                            <form:input path="passwordAgain" type="password" class="form-control"
                                        id="passwordAgain" placeholder=""/>
                            <form:errors path="passwordAgain" class="control-label"/>
                        </div>
                    </div>
                </spring:bind>
            </div>
            <div class=" ">
                <div class="form-group">
                    <div class="">
                        <button type="submit" class="btn-lg btn-primary pull-right"><spring:message
                                code="common.form.submit"/>
                        </button>
                    </div>
                </div>
            </div>
        </form:form>
    </div>
</div>

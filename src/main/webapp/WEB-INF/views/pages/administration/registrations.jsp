<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <div class="row">
        <div class="col-lg-12"><h2><spring:message code="administration.registrations.title"/></h2></div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive registrations-table">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th></th>
                            <th>ID lístku</th>
                            <th>Jméno</th>
                            <th>E-mail</th>
                            <th>Lístek</th>

                            <th>Ovládání</th>
                        </tr>
                    </thead>
                    <tbody>

                    <c:forEach items="${data}" var="registration">

                        <tr class="${registration.paid? 'paid-registration' : ''}">
                            <td><c:if test="${registration.createdToday}">
                                <span class="label label-success">New</span>
                            </c:if></td>
                            <td>666${registration.id}</td>
                            <td>${registration.firstName}&nbsp;${registration.surname}</td>
                            <td>${registration.email}</td>
                            <td><spring:message code="ticket.${registration.ticket.code}"/></td>


                            <td><c:if test="${!registration.paid}"><a class="btn" href="/administration/registration/paid/${registration.id}"> <i class="glyphicon glyphicon-ok"></i>Zaplaceno</a></c:if></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <div class="row">
        <div class="col-lg-12"><h2><spring:message code="administration.home.title"/></h2></div>
    </div>
<div class="row">
    <div class="gauge col-lg-2 col-md-2 col-sm-6 col-xs-6"><div class="circle" id="numOfRegistered" data-value="${data.numOfRegistered}"></div><h3>Počet<br/>registrovaných</h3></div>
    <div class="gauge col-lg-2 col-md-2 col-sm-6 col-xs-6"><div class="circle" id="numOfPaidRegistrations" data-value="${data.numOfPaidRegistrations}"></div><h3>Počet zaplacených<br/>registrací</h3></div>
    <div class="gauge col-lg-2 col-md-2 col-sm-6 col-xs-6"><div class="circle" id="numOfUnpaidRegistrations" data-value="${data.numOfRegistered - data.numOfPaidRegistrations}"></div><h3>Počet nezaplacených<br/>registrací</h3></div>
    <div class="gauge col-lg-2 col-md-2 col-sm-6 col-xs-6"><div class="circle" id="numOfActivities" data-value="${data.numOfActivities}"></div><h3>Počet<br/>aktivit</h3></div>
</div>

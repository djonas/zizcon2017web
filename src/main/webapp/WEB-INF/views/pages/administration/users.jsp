<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <div class="row">
        <div class="col-lg-12"><h2><spring:message code="administration.users.title"/></h2></div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive registrations-table">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Jméno</th>
                            <th>E-mail</th>
                            <th>Rok narození</th>
                            <th>Ovládání</th>
                        </tr>
                        </thead>
                        <tbody>

                        <c:forEach items="${data}" var="user">
                            <tr>
                                <td>${user.firstName}&nbsp;${user.surname}</td>
                                <td>${user.email}</td>
                                <td>${user.yearOfBirth}</td>


                                <td><a class="btn" href="/administration/users/resetPassword/${user.id}"> <i class="glyphicon glyphicon-repeat"></i>Reset hesla</a></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
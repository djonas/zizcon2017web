<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 3.6.2017
  Time: 18:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<section class="subpage">
<!--div class="container" id="registration">
    <c:choose>
        <c:when test="${empty success}">
            <div class="center section-text"><spring:message code="registration.sectionText"/>
            </div>
            <div class="row">
                <div class="col-md-8">

                    <spring:url value="/registration" var="userActionUrl"/>
                    <form:form method="post" modelAttribute="userRegistration" action="/registration" accept-charset="UTF-8">

                        <spring:bind path="ticket">
                            <spring:message code="registration.form.select.empty" var="select"/>
                            <div class="row">
                                <div class="form-group required ${status.error ? 'has-error' : ''}">
                                    <label class="col-sm-2 control-label"><spring:message
                                            code="registration.form.ticket"/></label>
                                    <div class="col-sm-10">
                                        <form:select path="ticket" class="form-control">
                                            <form:option value="NONE" label="${select}"/>
                                            <c:forEach items="${tickets}" var="currTicket">
                                                <spring:message code="ticket.${currTicket.code}" var="currItemLabel"/>
                                                <form:option value="${currTicket.code}" label="${currItemLabel}"/>
                                            </c:forEach>

                                        </form:select>
                                        <form:errors path="ticket" class="control-label"/>
                                    </div>
                                    <div class="col-sm-5"></div>
                                </div>
                            </div>
                        </spring:bind>

                        <form:hidden path="id"/>
                        <div class="row">
                        <spring:bind path="firstName">
                            <spring:message code="registration.form.firstName" var="firstName"/>
                            <div class="form-group required ${status.error ? 'has-error' : ''}">
                                <label class="col-sm-2 control-label">${firstName}</label>
                                <div class="col-sm-10">
                                    <form:input path="firstName" type="text" class="form-control"
                                                id="firstName" placeholder="${firstName}"/>
                                    <form:errors path="firstName" class="control-label"/>
                                </div>
                            </div>
                            </div>
                        </spring:bind>
                        <spring:bind path="surname">
                            <spring:message code="registration.form.surName" var="surName"/>
                            <div class="row">
                                <div class="form-group required ${status.error ? 'has-error' : ''}">
                                    <label class="col-sm-2 control-label">${surName}</label>
                                    <div class="col-sm-10">
                                        <form:input path="surname" type="text" class="form-control"
                                                    id="surname" placeholder="${surName}"/>
                                        <form:errors path="surname" class="control-label"/>
                                    </div>
                                </div>
                            </div>
                        </spring:bind>

                        <spring:bind path="nickname">
                            <spring:message code="registration.form.nickname" var="nickname"/>
                            <div class="row">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <label class="col-sm-2 control-label">${nickname}</label>
                                    <div class="col-sm-10">
                                        <form:input path="nickname" class="form-control"
                                                    id="nickname" placeholder="${nickname}"/>
                                        <form:errors path="nickname" class="control-label"/>
                                    </div>
                                </div>
                            </div>
                        </spring:bind>

                        <spring:bind path="email">
                            <div class="row">
                                <div class="form-group required ${status.error ? 'has-error' : ''}">
                                    <label class="col-sm-2 control-label">E-mail</label>
                                    <div class="col-sm-10">
                                        <form:input path="email" class="form-control"
                                                    id="email" placeholder="E-mail"/>
                                        <form:errors path="email" class="control-label"/>
                                    </div>
                                </div>
                            </div>
                        </spring:bind>

                        <spring:bind path="yearOfBirth">
                            <div class="row">
                                <div class="form-group required ${status.error ? 'has-error' : ''}">
                                    <label class="col-sm-2 control-label"><spring:message
                                            code="registration.form.yearOfBirth"/></label>
                                    <div class="col-sm-10">
                                        <form:input path="yearOfBirth" class="form-control"
                                                    id="yearOfBirth" placeholder="Year of birth"/>
                                        <form:errors path="yearOfBirth" class="control-label"/>
                                    </div>
                                </div>
                            </div>
                        </spring:bind>

                        <spring:bind path="phoneNumber">
                            <spring:message code="registration.form.phoneNumber" var="phoneNumber"/>
                            <div class="row">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <label class="col-sm-2 control-label">${phoneNumber}</label>
                                    <div class="col-sm-10">
                                        <form:input path="phoneNumber" class="form-control"
                                                    id="phoneNumber" placeholder="${phoneNumber}"/>
                                        <form:errors path="phoneNumber" class="control-label"/>
                                    </div>
                                </div>
                            </div>
                        </spring:bind>

                        <spring:bind path="gender">
                            <div class="row">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <label class="col-sm-2 control-label"><spring:message
                                            code="registration.form.gender"/></label>
                                    <div class="col-sm-10">
                                        <label class="radio-inline">
                                            <form:radiobutton path="gender" value="M"/> <spring:message
                                                code="common.gender.M"/>
                                        </label>
                                        <label class="radio-inline">
                                            <form:radiobutton path="gender" value="F"/> <spring:message
                                                code="common.gender.F"/>
                                        </label> <br/>
                                        <label class="radio-inline">
                                            <form:radiobutton path="gender" value="O"/> <spring:message
                                                code="common.gender.O"/>
                                        </label> <br/>
                                        <form:errors path="gender" class="control-label"/>
                                    </div>
                                </div>
                            </div>
                        </spring:bind>




                        <spring:bind path="street">
                            <spring:message code="registration.form.street" var="street"/>
                            <div class="row">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <label class="col-sm-2 control-label">${street}</label>
                                    <div class="col-sm-10">
                                        <form:input path="street" type="text" class="form-control"
                                                    id="street" placeholder="${street}"/>
                                        <form:errors path="street" class="control-label"/>
                                    </div>
                                </div>
                            </div>
                        </spring:bind>
                        <spring:bind path="city">
                            <spring:message code="registration.form.city" var="city"/>
                            <div class="row">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <label class="col-sm-2 control-label">${city}</label>
                                    <div class="col-sm-10">
                                        <form:input path="city" type="text" class="form-control"
                                                    id="city" placeholder="${city}"/>
                                        <form:errors path="city" class="control-label"/>
                                    </div>
                                </div>
                            </div>
                        </spring:bind>

                        <spring:bind path="zipCode">
                            <spring:message code="registration.form.zipCode" var="zipCode"/>
                            <div class="row">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <label class="col-sm-2 control-label">${zipCode}</label>
                                    <div class="col-sm-10">
                                        <form:input path="zipCode" class="form-control"
                                                    id="zipCode" placeholder="${zipCode}"/>
                                        <form:errors path="zipCode" class="control-label"/>
                                    </div>
                                </div>
                            </div>
                        </spring:bind>
                        <spring:bind path="acceptsTerms">
                    <div class="row">
                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                <spring:url value="/legal" var="legalUrl"/>
                                <label class="col-sm-8 col-lg-push-2 control-label"><spring:message code="registration.form.acceptsTerms" arguments="${legalUrl}"/></label>
                                <div class="col-sm-2">
                                    <div class="checkbox">
                                        <label>
                                            <form:checkbox path="acceptsTerms" id="acceptsTerms" />
                                        </label>
                                        <form:errors path="acceptsTerms" class="control-label" />
                                    </div>
                                </div>
                            </div>
                    </div>
                        </spring:bind>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-offset-2 col-md-12 col-lg-10 col-md-offset-2 ">
                                    <button type="submit" class="btn-lg btn-primary pull-right"><spring:message
                                            code="common.form.submit"/>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form:form>
                </div>
                <div class="col-md-4">
                    <div class="information-text">
                        <h3 class="red"><spring:message code="registration.information.tickets.header"/></h3>
                        <p><spring:message code="registration.information.tickets.text"/></p>
                    </div>

                    <div class="information-text">
                        <h3 class="red"><spring:message code="registration.information.discount.header"/></h3>
                        <p><spring:message code="registration.information.discount.text"/></p>
                    </div>
                    <div class="information-text">
                        <h3 class="red"><spring:message code="registration.information.payahead.header"/></h3>
                        <p><spring:message code="registration.information.payahead.text"/></p>
                        <table class="table">
                            <thead>
                            <tr class="bolder-text">
                                <th><spring:message code="registration.title"/></th>
                                <th><spring:message code="registration.information.payahead.before"/></th>
                                <th><spring:message code="registration.information.payahead.after"/></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><spring:message code="ticket.2017_FF_name"/></td>
                                <td>450 Kč</td>
                                <td>500 Kč</td>
                            </tr>
                            <tr>
                                <td><spring:message code="ticket.2017_FP_name"/></td>
                                <td>320 Kč</td>
                                <td>350 Kč</td>
                            </tr>
                            <tr>
                                <td><spring:message code="ticket.2017_14F_name"/></td>
                                <td>90 Kč</td>
                                <td>100 Kč</td>
                            </tr>
                            <tr>
                                <td><spring:message code="ticket.2017_14P_name"/></td>
                                <td>75 Kč</td>
                                <td>80 Kč</td>
                            </tr>
                            <tr>
                                <td><spring:message code="ticket.2017_15F_name"/></td>
                                <td>250 Kč</td>
                                <td>300 Kč</td>
                            </tr>
                            <tr>
                                <td><spring:message code="ticket.2017_15P_name"/></td>
                                <td>180 Kč</td>
                                <td>200 Kč</td>
                            </tr>
                            <tr>
                                <td><spring:message code="ticket.2017_16F_name"/></td>
                                <td>250 Kč</td>
                                <td>300 Kč</td>
                            </tr>
                            <tr>
                                <td><spring:message code="ticket.2017_16P_name"/></td>
                                <td>180 Kč</td>
                                <td>200 Kč</td>
                            </tr>
                            <tr>
                                <td><spring:message code="ticket.2017_17F_name"/></td>
                                <td>130 Kč</td>
                                <td>150 Kč</td>
                            </tr>
                            <tr>
                                <td><spring:message code="ticket.2017_17P_name"/></td>
                                <td>90 Kč</td>
                                <td>100 Kč</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </c:when>
        <c:otherwise>
            <c:set var="ticketNumber"><fmt:formatNumber minIntegerDigits="1" groupingUsed="false" value="${success.id}"/></c:set>
            <div class="center section-text wow fadeInUp"><spring:message code="registration.success.title" arguments="${ticketNumber}"/>
            </div>

            <div class="row wow fadeInUp">
                <div class="center">
                    <h3 class="red"><spring:message code="registration.success.paymentInfo.title" /></h3>
                    <p>
                        <spring:message code="registration.success.paymentInfo.details" arguments="${success.ticket.price},${ticketNumber}"/>
                    </p>
                    <p><spring:message code="registration.success.paymentInfo.email" arguments="${success.email}"/></p>
                </div>
            </div>
        </c:otherwise>
    </c:choose>


</div -->

<div class="container" id="registration">
    <div class="center section-text wow fadeInUp"><spring:message code="registration.closed.text" arguments="${ticketNumber}"/>
    </div>

    <div class="row wow fadeInUp">
        <div class="center">
            <h3 class="red"><spring:message code="registration.closed.title" /></h3>
        </div>
    </div>
</div>
</section>
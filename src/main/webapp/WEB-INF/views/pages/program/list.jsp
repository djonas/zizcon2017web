<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<section class="subpage" id="program">
    <div class="container-fluid program-controls">
        <div class="container">
            <div class="row">

                <div class="col-lg-8 col-md-6 col-sm-12 controls">
                    <form class="form-inline" role="form" method="get" action="/program/${view}" accept-charset="UTF-8" id="program-controls">
                        <div class="form-group">
                            <div class="input-group">
                                <label class="sr-only" for="type">Typ</label>
                                <div class="input-group-addon">Typ:</div>
                                <select class=" custom-select form-control mb-2 mr-sm-2 mb-sm-0" id="#type" name="type">
                                    <option ${empty type? 'selected' : ''} value=""><spring:message code="program.controls.all"/></option>
                                    <option ${type eq 'BG'? 'selected' : ''} value="BG"><spring:message code="homepage.aboutus.activity.boardgame.title"/></option>
                                    <option ${type eq 'RPG'? 'selected' : ''} value="RPG"><spring:message code="homepage.aboutus.activity.rpg.title"/></option>
                                    <option ${type eq 'LARP'? 'selected' : ''} value="LARP"><spring:message code="homepage.aboutus.activity.larp.title"/></option>
                                    <option ${type eq 'OTH'? 'selected' : ''} value="OTH"><spring:message code="homepage.aboutus.activity.other.title"/></option>
                                </select>
                            </div>
                        </div>
                        <sec:authorize access="hasRole('ROLE_USER')">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="form-check-input" name="myActivities" value="true" ${myActivities ? 'checked' : ''}>
                                    <spring:message code="program.controls.myActivities"/>
                                </label>
                            </div>
                        </sec:authorize>

                    </form>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-12 text-right views">
                    <a href="/program/list?type=${param['type']}&myActivities=${param['myActivities']}" class="btn active"><spring:message code="program.view.list"/></a>
                    <a href="/program/table?type=${param['type']}&myActivities=${param['myActivities']}" class="btn "><spring:message code="program.view.table"/></a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <c:choose>
            <c:when test="${page.totalPages eq 0}">
                <c:set var="emptyText" value="homepage.aboutus.activity"/>
                <div class="center section-text"><span class="bolder-text">
                    <c:choose>
                        <c:when test="${myActivities}">
                            <spring:message code="program.list.notAttending_${type}"/>
                        </c:when>
                        <c:otherwise>
                            <spring:message code="program.list.empty_${type}"/>
                        </c:otherwise>
                    </c:choose>
                </span>
                        ${errorMsg}
                </div>
            </c:when>

            <c:otherwise>
                <div class="col-lg-8">
                    <c:forEach items="${activities}" var="activity">
                        <div class="activity-item-background"
                             style="background: linear-gradient(rgba(0,0,0, .6), rgba(0,0,0,.6)),url('/image/${activity.stringId}'); background-size: auto,cover; background-position: center;">
                            <div class="activity-item-container">
                                <div class="activity-item-header ${activity.type}">
                                    <div class="activity-item-type">
                                        <spring:message code="program.list.item.type.${activity.type}"/>
                                    </div>
                                    <div class="activity-item-time">
                                        <h4><fmt:formatDate pattern="EEEE dd" value="${activity.from}"/>.&nbsp;<fmt:formatDate pattern="HH:mm" value="${activity.from}"/>-<fmt:formatDate pattern="HH:mm"
                                                                                                                                                                                     value="${activity.to}"/></h4>
                                    </div>
                                </div>
                                <div class="activity-item-data">
                                    <c:choose>
                                        <c:when test="${activity.maxAttendees > 0}">
                                            <h3> ${activity.title} (${activity.attendees}/${activity.maxAttendees})</h3>
                                        </c:when>
                                        <c:otherwise>
                                            <h3> ${activity.title}</h3>
                                        </c:otherwise>
                                    </c:choose>
                                    <span class="activity-item-summary">${activity.summary}</span>
                                    <div class="activity-item-buttons">
                                        <sec:authorize access="hasRole('ROLE_USER')">
                                            <c:choose>
                                                <c:when test="${activity.attendable}">
                                                    <a class="btn attend-button attend" data-url="/user/action/attend?activityId=${activity.stringId}"><spring:message code="program.list.info.sign"/></a>
                                                </c:when>
                                                <c:otherwise>
                                                    <c:choose>
                                                        <c:when test="${activity.reasonNotAttendable eq 'activity.notAttendable.alreadyAttending'}">
                                                            <a class="btn attend-button" data-url="/user/action/cancelAttendance?activityId=${activity.stringId}"><spring:message code="program.list.info.signout"/></a>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <span class="activity-item-warning"><i class="glyphicon glyphicon-warning-sign"></i><spring:message code="${activity.reasonNotAttendable}"/></span>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:otherwise>
                                            </c:choose>
                                        </sec:authorize>
                                        <a class="btn" href="/program/detail/${activity.stringId}"><spring:message code="program.list.info.detail"/></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </c:forEach>
                    <jsp:include page="../../../tiles/template/pagination.jsp"/>
                </div>
                <div class="col-lg-4">
                    <div class="information-text">
                        <h3 class="red"><spring:message code="program.list.info.title"/></h3>
                        <p><spring:message code="program.list.info.text"/></p>
                    </div>
                </div>
            </c:otherwise>

        </c:choose>

    </div>

</section>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<style>
.subpageHeader{
    display: none;

}
h1 {
    font-family: "Radikal Black", "Helvetica Neue", Helvetica, Arial, sans-serif;

    font-weight: bold;
    font-size: 60px;
    margin-bottom: 20px;
    color: #ff2800;
}
.activity-item-image {
    min-height: 500px ;
    background: url('/image/${activity.stringId}');
    background-size: cover;
    background-position: top;
}

.subpageHeader h1{
    color: #fff;
    text-shadow:0 3px 10px #000;
}
.activity-item-type{
    margin-left: 10px;
}
#activity-detail {
    margin-top: 175px;
}
@media (max-width: 990px) {
    .activity-item-image{
        min-height: 350px;
    }
    h1 {
        text-align: center;
        margin-top: 0;
        font-weight: bold;
        font-size: 40px;
        margin-bottom: 14px;
    }
}

@media ( max-width: 1366px ) {
    h1{
        font-size: 42px;
    }
    #activity-detail {
        margin-top: 125px;
    }
}
</style>
<section class="subpage" id="activity-detail">
    <div class="container">
        <div class="row activity-item-title">
            <h1>${activity.title}</h1>
        </div>
        <div class="row activity-item-image col-lg-12">

        </div>
            <div class="col-lg-12 activity-item-header ${activity.type}">
                <div class="activity-item-type">
                    <spring:message code="program.list.item.type.${activity.type}"/>
                </div>
                <div class="activity-item-time">
                    <h4><fmt:formatDate pattern="EEEE dd" value="${activity.from}"/>.&nbsp;<fmt:formatDate pattern="HH:mm" value="${activity.from}"/>-<fmt:formatDate pattern="HH:mm" value="${activity.to}"/></h4>
                </div>
            </div>

    </div>
    <div class="container activity-data">
        <div class="center section-text">${activity.summary}
        </div>
        <div class="row">
            <div class="col-md-8 col-sm-12 ">
                ${activity.description}
            </div>
            <div class="col-md-4 col-sm-12 activity-side-bar">
                <div>
                    <h4><spring:message code="program.detail.organizer"/></h4>
                    <h3>${activity.organizer}</h3>
                </div>
                <div class="activity-item-buttons">
                    <sec:authorize access="hasRole('ROLE_USER')">
                        <c:choose>
                            <c:when test="${activity.attendable}">
                                <a class="btn attend-button attend" data-url="/user/action/attend?activityId=${activity.stringId}"><spring:message code="program.list.info.sign"/></a>
                            </c:when>
                            <c:otherwise>
                                <c:choose>
                                    <c:when test="${activity.reasonNotAttendable eq 'activity.notAttendable.alreadyAttending'}">
                                        <a class="btn attend-button" data-url="/user/action/cancelAttendance?activityId=${activity.stringId}"><spring:message code="program.list.info.signout"/></a>
                                    </c:when>
                                    <c:otherwise>
                                        <span class="activity-item-warning"><i class="glyphicon glyphicon-warning-sign"></i><spring:message code="${activity.reasonNotAttendable}"/></span>
                                    </c:otherwise>
                                </c:choose>
                            </c:otherwise>
                        </c:choose>
                    </sec:authorize>
                </div>

                <div class="attendance">
                    <c:choose>
                        <c:when test="${not empty activity.limits}">
                            <c:forEach items="${activity.limits}" var="limit">
                                <c:choose>
                                    <c:when test="${fn:length(activity.limits) eq 1 and limit.value.name eq 'A'}">
                                        <i class="glyphicon glyphicon-user"></i><spring:message code="activity.limit.onelimit"/> ${limit.value.used}/${limit.value.max}<br>
                                    </c:when>
                                    <c:otherwise>
                                        <i class="glyphicon glyphicon-user"></i><spring:message code="activity.limit_${limit.key}"/> ${limit.value.used}/${limit.value.max}<br>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </c:when>
                        <c:otherwise>
                            <i class="glyphicon glyphicon-ok"></i><spring:message code="activity.limit.nolimit"/>
                        </c:otherwise>
                    </c:choose>

                </div>

            </div>
        </div>
    </div>
</section>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<section class="subpage" id="program">
    <div class="container-fluid program-controls">
        <div class="container">
            <div class="row">

                <div class="col-lg-8 col-md-6 col-sm-12 controls">
                    <form class="form-inline" role="form" method="get" action="/program/${view}" accept-charset="UTF-8" id="program-controls">
                        <div class="form-group">
                            <div class="input-group">
                                <label class="sr-only" for="type">Typ</label>
                                <div class="input-group-addon">Typ:</div>
                                <select class=" custom-select form-control mb-2 mr-sm-2 mb-sm-0" id="type" name="type">
                                    <option ${empty type? 'selected' : ''} value=""><spring:message code="program.controls.all"/></option>
                                    <option ${type eq 'BG'? 'selected' : ''} value="BG"><spring:message code="homepage.aboutus.activity.boardgame.title"/></option>
                                    <option ${type eq 'RPG'? 'selected' : ''} value="RPG"><spring:message code="homepage.aboutus.activity.rpg.title"/></option>
                                    <option ${type eq 'LARP'? 'selected' : ''} value="LARP"><spring:message code="homepage.aboutus.activity.larp.title"/></option>
                                    <option ${type eq 'OTH'? 'selected' : ''} value="OTH"><spring:message code="homepage.aboutus.activity.other.title"/></option>
                                </select>
                            </div>
                        </div>
                        <sec:authorize access="hasRole('ROLE_USER')">
                            <div class="checkbox">
                                <label>
                                    <input id="myActivities"  type="checkbox" class="form-check-input" name="myActivities" value="true" ${myActivities ? 'checked' : ''}>
                                    <spring:message code="program.controls.myActivities"/>
                                </label>
                            </div>
                        </sec:authorize>

                    </form>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-12 text-right views">
                    <a href="/program/list?type=${param['type']}&myActivities=${param['myActivities']}" class="btn "><spring:message code="program.view.list"/></a>
                    <a href="/program/table?type=${param['type']}&myActivities=${param['myActivities']}" class="btn active"><spring:message code="program.view.table"/></a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="center section-text"><spring:message code="program.table.about"/>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 legend">
                <div style="background-color: #7DD8CB"><spring:message code="homepage.aboutus.activity.boardgame.title"/></div>

            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 legend">
                <div style="background-color: #ff2800; color:#fff"><spring:message code="homepage.aboutus.activity.rpg.title"/></div>

            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 legend">
                <div style="background-color: #fff000"><spring:message code="homepage.aboutus.activity.larp.title"/></div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 legend">
                <div style="background-color: #000; color:#fff"><spring:message code="homepage.aboutus.activity.other.title"/></div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 legend">
                <div style="background-color: #26A65B"><spring:message code="activity.attending"/></div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 legend">
                <div style="background-color: #f2f2f2"><spring:message code="activity.cannotAttend"/></div>
            </div>

        </div>
    </div>
    <div class="container-fluid">

        <div class="row timetable">
            <div class="col-lg-12">
                <div class="timetable" id="visualization" data-locale="${locale.language}"></div>
            </div>
        </div>
    </div>

</section>

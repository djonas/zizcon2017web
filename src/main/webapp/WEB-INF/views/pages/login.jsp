<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="contaier">
    <div class="row">
        <div class="">
            <form class="form-signin" action="/login" method="post">
                <c:if test="${param.error != null}">
                    <p>
                        <spring:message code="login.invalid" />
                    </p>
                </c:if>
                <c:if test="${param.logout != null}">
                    <p>
                        <spring:message code="login.loggedout" />
                    </p>
                </c:if>
                <h3 class="form-signin-heading"> <spring:message code="login.please" /></h3>
                <label for="username" class="sr-only"> <spring:message code="login.username" /></label>
                <input type="text"  id="username" name="username" class="form-control" required="" autofocus="" placeholder="<spring:message code="login.username" />">
                <label for="password" class="sr-only"> <spring:message code="login.password" /></label>
                <input type="password" id="password" name="password" class="form-control" placeholder="<spring:message code="login.password" />" required="">
                <input type="hidden"
                       name="${_csrf.parameterName}"
                       value="${_csrf.token}"/>
                <button class="btn btn-lg btn-primary btn-block" type="submit"><spring:message code="login.login" /></button>
            </form>


        </div>
    </div>
</div>
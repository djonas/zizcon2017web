<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<tiles:importAttribute name="pageName"/>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <c:choose>
        <c:when test="${empty specificTitle}">
            <c:set var="pageName" scope="request"><tiles:getAsString name="pageName"/></c:set>
            <c:set var="ogTitle"><spring:message code="${pageName}.title" /></c:set>
            <title>Žižcon - <spring:message code="${pageName}.title" /></title>
        </c:when>
        <c:otherwise>
            <c:set var="specificTitle" scope="request">${specificTitle}</c:set>
            <c:set var="ogTitle">${specificTitle}</c:set>
            <title>Žižcon - ${specificTitle}</title>
        </c:otherwise>
    </c:choose>
    <meta name="description" content="Festival deskových a rolových her na Žižkově."/>
    <meta name="keywords"
          content="Žižkov,Praha,deskovky,deskové hry,karetní hry,LARPy,LCG,CCG,TCG,události,akce,workshopy,rodina,zábava,dění,LARP,rpg,d&d,festival"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" href="/resources/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/resources/css/bootstrap-theme.min.css">

    <link rel="stylesheet" href="/resources/css/animate.min.css">

    <link rel="stylesheet" href="/resources/css/vis.min.css">
    <link rel="stylesheet" href="/resources/css/vis-timeline-graph2d.min.css">

    <tiles:importAttribute name="stylesheets"/>
    <c:forEach var="css" items="${stylesheets}">
        <link rel="stylesheet" type="text/css" href="${css}"/>
    </c:forEach>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

    <link rel="apple-touch-icon" sizes="57x57" href="/resources/img/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/resources/img/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/resources/img/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/resources/img/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/resources/img/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/resources/img/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/resources/img/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/resources/img/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/resources/img/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/resources/img/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/resources/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/resources/img/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/resources/img/favicon-16x16.png">
    <link rel="shortcut icon" type="image/x-icon" href="/resources/img/favicon.ico" />
    <link rel="manifest" href="/resources/img/manifest.json">

    <meta property="og:title" content="Žižcon - ${ogTitle}"/>
    <meta property="og:image" content="http://www.zizcon.cz/resources/img/fcb_icon.jpg"/>
    <meta property="og:description" content="Festival deskových a rolových her na Žižkově"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="http://www.zizcon.cz"/>

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">


    <script src="/resources/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

</head>
<body>
<!-- Header -->
<tiles:insertAttribute name="header"  />
<!-- Body -->
<tiles:insertAttribute name="body" />
<!-- Footer -->
<tiles:insertAttribute name="footer" />

<div class="modal fade" id="zizconModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" id="modalBody">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</body>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/resources/js/vendor/jquery-1.11.2.js"><\/script>')</script>
<script src="/resources/js/vendor/bootstrap.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
<script src="/resources/js/vendor/owl.carousel.js"></script>
<script src="/resources/js/vendor/wow.min.js"></script>
<script src="/resources/js/vendor/vis.min.js"></script>
<script src="/resources/js/vendor/vis-timeline-graph2d.min.js"></script>
<script src="/resources/js/vendor/progressbar.js"></script>
<tiles:importAttribute name="javascripts" />
<c:forEach var="javascript" items="${javascripts}">
    <script src="${javascript}"></script>
</c:forEach>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCCyND7B4QqLSMlYEKU6oqWKFaQJMSWkjM"></script>
<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X','auto');ga('send','pageview');
</script>
</html>

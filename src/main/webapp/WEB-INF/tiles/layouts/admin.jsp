<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="container-fluid">
    <div class="row logo">
        <div class="col-lg-12"> <img src="/resources/img/logo_admin.png"></div>

    </div>
    <div class="row profile">
        <div class="col-md-3">
            <div class="profile-sidebar">
                <!-- SIDEBAR USERPIC -->

                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        <sec:authentication property="principal.username" />
                    </div>
                </div>
                <!-- END SIDEBAR USER TITLE -->

                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu">

                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <ul class="nav">
                                <li class="${pageName eq 'administration.home'? 'active': ''}">
                                    <a href="/administration">
                                        <i class="glyphicon glyphicon-home"></i>
                                        <spring:message code="administration.home.title"/></a>
                                </li>

                                <li class="${pageName eq 'administration.registrations'? 'active': ''}">
                                    <a href="/administration/registrations">
                                        <i class="glyphicon glyphicon-file"></i>
                                        <spring:message code="administration.registrations.title"/></a>
                                </li>
                                <li class="${pageName eq 'administration.users'? 'active': ''}">
                                    <a href="/administration/users">
                                        <i class="glyphicon glyphicon-user"></i>
                                        <spring:message code="administration.users.title"/></a>
                                </li>
                                <li class="${pageName eq 'administration.activities'? 'active': ''}">
                                    <a href="/administration/activities">
                                        <i class="glyphicon glyphicon-time"></i>
                                        <spring:message code="administration.activities.title"/></a>
                                </li>

                            </ul>
                        </sec:authorize>
                        <sec:authorize access="hasRole('ROLE_USER')">
                            <ul class="nav">
                            <li class="${pageName eq 'profile.home'? 'active': ''}">
                                <a href="/profile">
                                    <i class="glyphicon glyphicon-user"></i>
                                    <spring:message code="profile.home.title"/></a>
                            </li>
                            <li class="${pageName eq 'profile.myActivities'? 'active': ''}">
                                <a href="/profile/my-activities">
                                    <i class="glyphicon glyphicon-tasks"></i>
                                    <spring:message code="profile.myActivities.title"/></a>
                            </li>
                            </ul>
                        </sec:authorize>
                    <!-- SIDEBAR BUTTONS -->
                    <div class="profile-userbuttons">
                        <a href="/" class="btn "><spring:message code="common.menu.backtoweb"/></a>
                        <a href="/logout" class="btn "><spring:message code="common.menu.logout"/></a>
                    </div>
                    <!-- END SIDEBAR BUTTONS -->
                </div>
                <!-- END MENU -->
            </div>
        </div>
        <div class="col-md-9">
            <div class="admin-content">
                <tiles:insertAttribute name="admin-content" />
            </div>
        </div>
    </div>
</div>
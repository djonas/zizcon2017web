<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<footer class="nb-footer">
    <div class="container">
        <div class ="row">
            <div class="col-sm-12 footer-header">
            <h2><spring:message code="common.footer.header" /></h2>
                </div>
        </div>
        <div class="row">
            <c:if test="${not empty footerNews}">
                <c:forEach items="${footerNews}" var="newsItem">
                    <div class="col-md-3 col-sm-6">
                        <div class="footer-info-single">
                            <h3 class="title red">${newsItem.title}</h3>
                            <p>${newsItem.summary}</p>
                            <p class="date"> <fmt:formatDate pattern="dd.MM.yyyy" value = "${newsItem.published}" /></p>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
            <div class="col-sm-12">

                <div class="about">
                    <p><spring:message code="common.footer.socialMedia" /></p>
                    <div class="social-media">
                        <ul class="list-inline">
                            <li><a href="https://www.facebook.com/festivalzizcon/" title=""><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" title=""><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <section class="copyright">
        <div class="container">
            <div class="row">
                <div class="">
                    <a href="#header"><img class="img-responsive center-block" src="/resources/img/logo_yellow.png"></a>
                </div>
            </div>
        </div>
    </section>
</footer>
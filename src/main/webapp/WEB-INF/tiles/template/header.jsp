<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container" >
        <div class="row" style="padding: 0">

            <sec:authorize access="isAnonymous()">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 languages">

                    <a href="<my:replaceParam name='locale' value='cs' />">cs</a>
                    <a href="<my:replaceParam name='locale' value='en' />">en</a>

                </div>
                <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6 login">
                    <a href="/login"><spring:message code="common.menu.login"/></a>
                </div>
            </sec:authorize>
            <sec:authorize access="hasRole('ROLE_ADMIN')">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 languages">

                    <a href="<my:replaceParam name='locale' value='cs' />">cs</a>
                    <a href="<my:replaceParam name='locale' value='en' />">en</a>

                </div>
                <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6 login">
                    <a href="/administration">administrace</a>
                    <a href="/logout">odhlásit</a>
                </div>
            </sec:authorize>
            <sec:authorize access="hasRole('ROLE_USER')">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 languages">

                    <a href="<my:replaceParam name='locale' value='cs' />">cs</a>
                    <a href="<my:replaceParam name='locale' value='en' />">en</a>

                </div>
                <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6  hidden-xs login">
                    <spring:message code="common.header.welcome"/>&nbsp; <sec:authentication property="principal.username" />&nbsp;<a href="/profile"><spring:message code="common.menu.profile"/></a>
                    <a href="/logout"><spring:message code="common.menu.logout"/></a>
                </div>
            </sec:authorize>
        </div>



        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-brand-centered">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="/">
                <div class="navbar-brand navbar-brand-centered"><img src="/resources/img/logo.png"
                                                                     alt="Žižcon"></div>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-brand-centered">
            <ul class="nav navbar-nav navbar-left">
                <li><a href="/program/table"><spring:message code="common.menu.program"/></a></li>
                <li><a href="/#where"><spring:message code="common.menu.place"/></a></li>
                <li><a href="/aboutus/news"><spring:message code="common.menu.aboutus.news"/></a></li>

            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/registration"><spring:message code="common.menu.tickets"/></a></li>
                <li><a href="/rules"><spring:message code="common.menu.rules"/></a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><spring:message
                            code="common.menu.aboutus"/><span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="/aboutus/whoarewe"><spring:message
                                code="common.menu.aboutus.who"/></a></li>
                        <li><a href="/aboutus/team"><spring:message
                                code="common.menu.aboutus.team"/></a></li>
                        <li><a href="/aboutus/anotherActivities"><spring:message
                                code="common.menu.aboutus.activities"/></a></li>
                        <li><a href="/aboutus/news"><spring:message
                                code="common.menu.aboutus.news"/></a></li>
                        <!--li class="divider"></li-->
                    </ul>
                </li>
                <li class="visible-xs"><a href="/profile">Profil</a></li>
                <li class="visible-xs"><a href="/logout">odhlásit</a></li>
            </ul>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<c:choose>
    <c:when test="${showHero eq true}">
        <header role="banner" id="header" class="header">
            <div class="hero wow fadeInUp">
                <h1><spring:message code="homepage.header.title"/></h1>
                <p><spring:message code="homepage.header.subtitle"/></p>
                <a href="/registration">
                    <div class="btn btn-primary"><spring:message code="homepage.header.button.tickets"/></div>
                </a>
                <a href="/program/table">
                <div class="btn btn-primary"><spring:message code="homepage.header.button.program"/></div>
                </a>
            </div>
        </header>
        <!-- header role="banner" -->
    </c:when>
    <c:otherwise>

        <header role="banner" id="header" class="subpageHeader">
            <div class="container">
                <h1>
                    <c:choose>
                    <c:when test="${empty specificTitle}">
                    <spring:message code="${pageName}.title"/></h1>
                </c:when>
                <c:otherwise>
                    ${specificTitle}
                </c:otherwise>
                </c:choose>
            </div>
        </header>
        <!-- header role="banner" -->
    </c:otherwise>


</c:choose>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="center">
    <div class="row">
        <ul class="pagination">
            <!-- FIRST -->
            <li class="${page.firstPage?'disabled':''}">
                <c:choose>
                    <c:when test="${not page.firstPage}">
                        <a href="${page.url}?${empty page.params? '' : page.params+='&'}page=0&size=${page.size}"><spring:message code="common.pagination.first"/></a>
                    </c:when>
                    <c:otherwise>
                        <span><spring:message code="common.pagination.first"/></span>
                    </c:otherwise>
                </c:choose>
            </li>
            <!-- PREVIOUS -->
            <li class="${page.hasPreviousPage?'':'disabled'}">
                <c:choose>
                    <c:when test="${page.hasPreviousPage}">
                        <a href="${page.url}?${empty page.params? '' : page.params+='&'}page=${page.number-2}&size=${page.size}">«</a>
                    </c:when>
                    <c:otherwise>
                        <span>«</span>
                    </c:otherwise>
                </c:choose>
            </li>
            <!-- ITEMS -->
            <c:forEach items="${page.items}" var="paginationItem">
                <li class="${paginationItem.current?'active':''}">
                    <c:choose>
                    <c:when test="${not paginationItem.current}">
                        <a href="${page.url}?${empty page.params? '' : page.params += '&'}page=${paginationItem.number-1}&size=${page.size}">${paginationItem.number}</a>
                    </c:when>
                    <c:otherwise>
                    <span>${paginationItem.number}<span>
                </c:otherwise>
            </c:choose>
                </li>

            </c:forEach>

            <!-- NEXT -->
            <li class="${page.hasNextPage?'':'disabled'}">
                <c:choose>
                    <c:when test="${page.hasNextPage}">
                        <a href="${page.url}?${empty page.params? '' : page.params+='&'}page=${page.number}&size=${page.size}">»</a>
                    </c:when>
                    <c:otherwise>
                        <span>»</span>
                    </c:otherwise>
                </c:choose>
            </li>
            <!-- LAST -->
            <li class="${page.lastPage?'disabled':''}">
                <c:choose>
                    <c:when test="${not page.lastPage}">
                        <a href="${page.url}?${empty page.params? '' : page.params+='&'}page=${page.totalPages -1}&size=${page.size}"><spring:message code="common.pagination.last"/></a>
                    </c:when>
                    <c:otherwise>
                        <span><spring:message code="common.pagination.last"/></span>
                    </c:otherwise>
                </c:choose>
            </li>
        </ul>

    </div>
</div>
package cz.jo.zizcon.dao;

import cz.jo.zizcon.model.entity.ActivityAttendee;
import org.springframework.data.repository.CrudRepository;

public interface ActivityAttendeeRepository extends CrudRepository<ActivityAttendee, Long> {

}

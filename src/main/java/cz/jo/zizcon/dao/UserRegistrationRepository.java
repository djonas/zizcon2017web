package cz.jo.zizcon.dao;

import cz.jo.zizcon.model.entity.UserRegistration;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by UserRegistration on 25.5.2017.
 */
public interface UserRegistrationRepository extends CrudRepository<UserRegistration, Integer> {

  public UserRegistration findByEmail(String email);

}

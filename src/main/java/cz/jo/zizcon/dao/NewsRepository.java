package cz.jo.zizcon.dao;

import cz.jo.zizcon.model.entity.News;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Created by User on 5.6.2017.
 */
public interface NewsRepository extends PagingAndSortingRepository<News, Integer> {

  Page<News> findByLanguageOrderByPublishedDesc(String language, Pageable pageable);

}

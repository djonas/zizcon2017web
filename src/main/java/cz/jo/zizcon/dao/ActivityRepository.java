package cz.jo.zizcon.dao;

import cz.jo.zizcon.model.entity.Activity;
import cz.jo.zizcon.model.entity.Activity.Type;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.support.PageableExecutionUtils;

/**
 * Created by User on 6.6.2017.
 */
public interface ActivityRepository extends PagingAndSortingRepository<Activity, Integer> {


  Activity findByStringId(String stringId);

  Page<Activity> findByActivityType(Type activityType, Pageable pageable);

  Page<Activity> findByAttendees_User_Username(String username, Pageable pageable);

  Page<Activity> findByActivityTypeAndAttendees_User_Username(Type activityType,String username, Pageable pageable);


}

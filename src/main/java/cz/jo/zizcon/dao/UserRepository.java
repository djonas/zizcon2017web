package cz.jo.zizcon.dao;

import cz.jo.zizcon.model.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

  User findByUsername(String username);

}

package cz.jo.zizcon.dao;

import cz.jo.zizcon.model.dto.TicketDTO;
import cz.jo.zizcon.model.entity.Ticket;
import java.util.Date;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by User on 3.6.2017.
 */
public interface TicketRepository extends CrudRepository<Ticket, Integer> {

  Ticket getByCode(String s);

  List<Ticket> findByRegistrableFromBeforeAndRegistrableToAfter(Date from, Date to);
}

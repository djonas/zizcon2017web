package cz.jo.zizcon.dao;

import cz.jo.zizcon.model.entity.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository  extends CrudRepository<Role, Long> {

  Role findByName(String name);
}

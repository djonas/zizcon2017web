package cz.jo.zizcon.mailing.impl;

import cz.jo.zizcon.mailing.MailingConstants;
import cz.jo.zizcon.mailing.MailingConstants.Templates;
import cz.jo.zizcon.mailing.MailingService;
import cz.jo.zizcon.model.dto.UserRegistrationDTO;
import cz.jo.zizcon.model.entity.User;
import freemarker.template.Configuration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;


@SuppressWarnings("deprecation")
@Service("mailService")
public class MailingServiceImpl implements MailingService {

  @Autowired
  private JavaMailSender mailSender;

  @Autowired
  private Configuration emailFreemarker;

  @Autowired
  private MessageSource messageSource;

  @Value("${zizcon.mailing.team.from}")
  String zizconMail;

  private static final Logger logger = LoggerFactory.getLogger(MailingServiceImpl.class);


  @Override
  public void sendRegistrationEmails(UserRegistrationDTO userReg) {
    Locale locale = LocaleContextHolder.getLocale();

    Map<String, Object> model = new HashMap<>();
    model.put("data", userReg);

    MimeMessagePreparator customerMail = getMessagePreparator(model, userReg.getEmail(), MailingConstants.Templates.CUSTOMER_REGISTRATION, locale);

    MimeMessagePreparator teamMail = getMessagePreparator(model, "info@zizcon.cz", MailingConstants.Templates.TEAM_INFORMATIVE, Locale.getDefault());
    try {
      mailSender.send(customerMail);
    } catch (MailException e) {
      logger.error(String.format("Could not send customer email to %s", userReg.getEmail()));
    }
    try {
      mailSender.send(teamMail);
    } catch (MailException e) {
      logger.error("Could not send informative email to zizcon team!");
    }

  }

  @Override
  public void sendPaidEmails(UserRegistrationDTO userReg, String username, String password) {
    Locale locale = LocaleContextHolder.getLocale();

    Map<String, Object> model = new HashMap<>();
    model.put("data", userReg);
    model.put("username", username);
    model.put("password", password)
    ;
    MimeMessagePreparator paidMail = getMessagePreparator(model, userReg.getEmail(), Templates.CUSTOMER_PAID, locale);
    try {
      mailSender.send(paidMail);
    } catch (MailException e) {
      logger.error(String.format("Could not send payment information email to %s", userReg.getEmail()));
    }
  }

  @Override
  public void sendResetPasswordEmail(User user, String newPassword) {
    Locale locale = LocaleContextHolder.getLocale();

    Map<String, Object> model = new HashMap<>();
    model.put("data", user);
    model.put("username", user.getUsername());
    model.put("password", newPassword);
    MimeMessagePreparator paidMail = getMessagePreparator(model, user.getEmail(), Templates.PASSWORD_RESET, locale);
    try {
      mailSender.send(paidMail);
    } catch (MailException e) {
      logger.error(String.format("Could not resseted password email to %s", user.getEmail()));
    }

  }

  private MimeMessagePreparator getMessagePreparator(final Map<String, Object> model, String email, String templateName, Locale locale) {
    return mimeMessage -> {
      MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

      helper.setSubject(getLocalizedAttribute(MailingConstants.Attributes.SUBJECT, templateName, locale));
      helper.setFrom(zizconMail);
      helper.setTo(email);


      String text = geFreeMarkerTemplateContent(model, templateName, locale);

      // use the true flag to indicate you need a multipart message
      helper.setText(text, true);
      if (logger.isDebugEnabled()) {
        logger.debug(String
            .format("Sending email to customer with address %s. Email body: %s", email, text));
      }
    };
  }

  private String geFreeMarkerTemplateContent(Map<String, Object> model, String templateName, Locale locale) {
    StringBuilder content = new StringBuilder();
    try {
      content.append(FreeMarkerTemplateUtils.processTemplateIntoString(
          emailFreemarker.getTemplate(getLocalizedTemplateName(templateName, locale)), model));
      return content.toString();
    } catch (Exception e) {
      logger.error("Exception occured while processing fmtemplate:" + e.getMessage());
    }
    return "";
  }

  private String getLocalizedTemplateName(String templateName, Locale locale) {
    return templateName +
        "_" +
        locale.getLanguage() +
        MailingConstants.Templates.DEFAULT_FILETYPE;
  }

  private String getLocalizedAttribute(String attribute, String template, Locale locale) {
    String messageAlias = MailingConstants.Attributes.PREFIX +
        template +
        attribute;
    return messageSource.getMessage(messageAlias, null, locale);
  }

}

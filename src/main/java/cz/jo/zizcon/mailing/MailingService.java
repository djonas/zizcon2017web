package cz.jo.zizcon.mailing;

import cz.jo.zizcon.model.dto.UserRegistrationDTO;
import cz.jo.zizcon.model.entity.User;

/**
 * Created by User on 3.6.2017.
 */
public interface MailingService {

  public void sendRegistrationEmails(UserRegistrationDTO userReg);

  public void sendPaidEmails(UserRegistrationDTO userReg, String username, String password);

  public void sendResetPasswordEmail(User user, String newPassword);

}

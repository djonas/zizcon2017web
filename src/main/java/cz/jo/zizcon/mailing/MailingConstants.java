package cz.jo.zizcon.mailing;

/**
 * Created by User on 3.6.2017.
 */
public interface MailingConstants {

  public interface Templates {

    public static final String CUSTOMER_REGISTRATION = "customer_email";
    public static final String CUSTOMER_PAID = "customer_paid_email";
    public static final String TEAM_INFORMATIVE = "informative_email";
    public static final String PASSWORD_RESET = "reset_password_email";
    public static final String DEFAULT_FILETYPE = ".html";
  }

  public interface Attributes {

    ///Prefix
    public static final String PREFIX = "email.";
    //Mail localized attributes
    public static final String SUBJECT = ".subject";
  }

}

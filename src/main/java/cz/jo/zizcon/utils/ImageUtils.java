package cz.jo.zizcon.utils;


import com.mortennobel.imagescaling.AdvancedResizeOp;
import com.mortennobel.imagescaling.ResampleOp;
import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

/**
 * Created by djonas on 23.3.14.
 */
public final class ImageUtils {

  static Logger logger = Logger.getLogger(ImageUtils.class);

  public static byte[] multipartToByteArray(MultipartFile file) {
    try {
      return file.getBytes();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static InputStream multipartToStreamInput(MultipartFile file) throws IOException {
    return file.getInputStream();
  }

  public static byte[] resizeToThumb(int x, int y, MultipartFile file) throws IOException {
    ResampleOp op = new ResampleOp(x, y);
    op.setUnsharpenMask(AdvancedResizeOp.UnsharpenMask.Normal);
    InputStream in = new ByteArrayInputStream(multipartToByteArray(file));
    BufferedImage originalImg = ImageIO.read(in);
    logger.debug("original image buffer" + originalImg.toString());

    int type = originalImg.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImg.getType();
    BufferedImage resizedImg = new BufferedImage(x, y, type);
    Graphics2D g = resizedImg.createGraphics();
    g.drawImage(originalImg, 0, 0, x, y, null);
    g.dispose();

    logger.debug("resized image buffer" + resizedImg.toString());

    ByteArrayOutputStream os = new ByteArrayOutputStream();
    ImageIO.write(resizedImg, "JPG", os);
    byte[] resizedBytes = os.toByteArray();
    logger.debug("resized bytes" + Arrays.toString(resizedBytes));
    return resizedBytes;


  }


}

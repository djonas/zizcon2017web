package cz.jo.zizcon.utils;

import cz.jo.zizcon.dao.RoleRepository;
import cz.jo.zizcon.dao.UserRepository;
import cz.jo.zizcon.model.entity.Role;
import cz.jo.zizcon.model.entity.User;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class InitialDataLoader implements
    ApplicationListener<ContextRefreshedEvent> {

  private boolean alreadySetup = false;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private RoleRepository roleRepository;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Override
  @Transactional
  public void onApplicationEvent(ContextRefreshedEvent event) {

    if (alreadySetup)
      return;

    createRoleIfNotFound("ROLE_ADMIN");
    createRoleIfNotFound("ROLE_USER");

    alreadySetup = createAdminIfNotFound();
  }

  private boolean createAdminIfNotFound() {
    if(userRepository.findByUsername("administrator") == null) {
      Role adminRole = roleRepository.findByName("ROLE_ADMIN");
      User user = new User();
      user.setFirstName("Administrátor");
      user.setSurname("Žižcon");
      user.setUsername("administrator");
      user.setPassword(passwordEncoder.encode("Koberec14"));
      user.setEmail("info@zizcon.cz");
      user.setYearOfBirth(1000);
      user.setRoles(Arrays.asList(adminRole));
      user.setEnabled(true);
      userRepository.save(user);
    }
    return true;
  }


  @Transactional
  Role createRoleIfNotFound(
      String name) {

    Role role = roleRepository.findByName(name);
    if (role == null) {
      role = new Role();
      role.setName(name);
      roleRepository.save(role);
    }
    return role;
  }
}
package cz.jo.zizcon.controller;

import cz.jo.zizcon.controller.util.PageWrapper;
import cz.jo.zizcon.model.dto.NewsDTO;
import cz.jo.zizcon.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import java.util.Locale;
import java.util.Optional;

/**
 * Created by User on 6.6.2017.
 */
@Controller
public class AboutusController {

  @Autowired
  NewsService newsService;

  @RequestMapping(value = {ControllerConstants.Mapping.ABOUTUS_ANOTHER_ACTIVITIES}, method = RequestMethod.GET)
  public String anotherActivities(ModelMap model) {
    return ControllerConstants.Tiles.ABOUTUS_ANOTHER_ACTIVITIES;
  }


  @RequestMapping(value = {ControllerConstants.Mapping.ABOUTUS_WHO_ARE_WE}, method = RequestMethod.GET)
  public String whoAreWe(ModelMap model) {
    return ControllerConstants.Tiles.ABOUTUS_WHO_ARE_WE;
  }

  @RequestMapping(value = {ControllerConstants.Mapping.ABOUTUS_TEAM}, method = RequestMethod.GET)
  public String team(ModelMap model) {
    return ControllerConstants.Tiles.ABOUTUS_TEAM;
  }


  @RequestMapping(value = {ControllerConstants.Mapping.ABOUTUS_NEWS}, method = RequestMethod.GET)
  public String news(ModelMap model, Locale locale, @PageableDefault(sort = {"published"}, value = 10) Pageable pageable) {
    Page<NewsDTO> newsPage = newsService.getAll(locale, pageable);
    PageWrapper<NewsDTO> page = new PageWrapper<>(newsPage, ControllerConstants.Mapping.ABOUTUS_NEWS);
    model.addAttribute("news", page.getContent());
    model.addAttribute("page", page);
    return ControllerConstants.Tiles.ABOUTUS_NEWS;
  }


  @RequestMapping(value = {ControllerConstants.Mapping.ABOUTUS_NEWS_DETAIL}, method = RequestMethod.GET)
  public String detail(@PathVariable("id") int id, Model model, Locale locale, RedirectAttributes redirectAttributes) {

    Optional<NewsDTO> newsDTO = newsService.getById(id);

    if (id == 0 || !newsDTO.isPresent()) {
      redirectAttributes.addFlashAttribute("error", "news.list.nonexistentNews");
      return "redirect:" + ControllerConstants.Tiles.ABOUTUS_NEWS;
    } else {
      model.addAttribute("article", newsDTO.get());
      model.addAttribute("specificTitle", newsDTO.get().getTitle());
      return ControllerConstants.Tiles.ABOUTUS_NEWS_DETAIL;
    }
  }


}

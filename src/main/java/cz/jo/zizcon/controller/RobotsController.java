package cz.jo.zizcon.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

@Controller
public class RobotsController {

  @RequestMapping(value = "/robots.txt", method = RequestMethod.GET)
  @ResponseBody
  public String getRobots(HttpServletRequest request) {
    return (Arrays.asList("zizcon.cz", "www.zizcon.cz", "localhost").contains(request.getServerName())) ?
        "robotsAllowed" : "robotsDisallowed";
  }
}
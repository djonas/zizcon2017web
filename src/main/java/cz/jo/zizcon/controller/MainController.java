package cz.jo.zizcon.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by User on 25.5.2017.
 */
@Controller
public class MainController {



  @RequestMapping(value = {ControllerConstants.Mapping.HOME}, method = RequestMethod.GET)
  public String homePage(ModelMap model) {
    model.addAttribute("showHero", true);
    return ControllerConstants.Tiles.HOMEPAGE;
  }

  @RequestMapping(value = {ControllerConstants.Mapping.LEGAL}, method = RequestMethod.GET)
  public String legalPage(ModelMap model) {
    return ControllerConstants.Tiles.LEGAL;
  }

  @RequestMapping(value = {ControllerConstants.Mapping.RULES}, method = RequestMethod.GET)
  public String rulesPage(ModelMap model) {
    return ControllerConstants.Tiles.RULES;
  }

  @RequestMapping(value = {ControllerConstants.Mapping.CONTACTS}, method = RequestMethod.GET)
  public String contactsPage(ModelMap model) {
    return ControllerConstants.Tiles.CONTACTS;
  }


}

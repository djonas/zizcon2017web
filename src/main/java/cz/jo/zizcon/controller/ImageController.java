package cz.jo.zizcon.controller;

/**
 * Created by User on 25.5.2017.
 */

import cz.jo.zizcon.service.ImageService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Controller
public class ImageController {

  Logger logger = Logger.getLogger(ImageController.class);

  @Autowired
  ImageService imageService;

  @RequestMapping(value = "/image/{id}", method = RequestMethod.GET)
  public void getBigImage(final HttpServletResponse response, @PathVariable("id") final String id) throws IOException {

    logger.debug("Retrieving image with id: " + id);
    writeFileContentToHttpResponse(imageService.getBigImageDataByID(id), response);
  }

  @RequestMapping(value = "/thumb/{id}", method = RequestMethod.GET)
  public void getThumbImage(final HttpServletResponse response, @PathVariable("id") final String id) throws IOException {

    logger.debug("Retrieving thumb with id: " + id);
    writeFileContentToHttpResponse(imageService.getThumbImageDataByID(id), response);
  }


  public static void writeFileContentToHttpResponse(final byte[] image, final HttpServletResponse response) throws IOException {

    ServletOutputStream out = response.getOutputStream();
    InputStream in = new ByteArrayInputStream(image);
    int length;
    int bufferSize = 1024;
    byte[] buffer = new byte[bufferSize];

    response.setContentType("image/jpeg");
    while ((length = in.read(buffer)) != -1) {
      out.write(buffer, 0, length);
    }
    in.close();
    out.flush();
  }
    /*
    @RequestMapping(value = "/postImage", method = RequestMethod.POST)
    public String postImage() throws IOException {
        logger.debug("Saving new image into db");
        //coverService.saveCoverArt(coverHolder.getCover(), 1);
        return "/filetest";
    }

    */
}

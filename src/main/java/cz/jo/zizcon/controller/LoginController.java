package cz.jo.zizcon.controller;

import cz.jo.zizcon.controller.ControllerConstants.Tiles;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by User on 20.6.2017.
 */
@Controller
public class LoginController {

  @RequestMapping(value = {ControllerConstants.Mapping.LOGIN}, method = RequestMethod.GET)
  public String login(ModelMap model) {
    return ControllerConstants.Tiles.LOGIN;
  }

  @RequestMapping(value = {ControllerConstants.Mapping.FORGOTTEN_PASSWORD}, method = RequestMethod.GET)
  public String forgottenPassword(ModelMap model) {
    return Tiles.FORGOTTEN_PASSWORD;
  }
}

package cz.jo.zizcon.controller.administration;

import static cz.jo.zizcon.controller.ImageController.writeFileContentToHttpResponse;

import cz.jo.zizcon.controller.ControllerConstants.Mapping.Administration;
import cz.jo.zizcon.controller.ControllerConstants.Tiles;
import cz.jo.zizcon.service.AdministrationService;
import cz.jo.zizcon.service.ChangePasswordService;

import cz.jo.zizcon.service.ExporterService;

import cz.jo.zizcon.service.UserService;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponents;

@Controller
public class AdministrationController {

  @Autowired
  AdministrationService administrationService;

  @Autowired
  UserService userService;

  @Autowired
  ExporterService exporterService;

  @Autowired
  ChangePasswordService changePasswordService;

  @RequestMapping(value = {Administration.HOME}, method = RequestMethod.GET)
  public String administrationHome(ModelMap model) {
    model.addAttribute("data", administrationService.getOverwiev());
    return Tiles.Administration.HOME;
  }

  @RequestMapping(value = {Administration.USERS}, method = RequestMethod.GET)
  public String administrationUsers(ModelMap model) {
    model.addAttribute("data", userService.findAll());
    return Tiles.Administration.USERS;
  }

  @RequestMapping(value = {Administration.ACTIVITIES}, method = RequestMethod.GET)
  public String administrationActivities(ModelMap model) {
    return Tiles.Administration.ACTIVITIES;
  }

  @RequestMapping(value = {Administration.REGISTRATIONS}, method = RequestMethod.GET)
  public String administrationRegistrations(ModelMap model) {
    model.addAttribute("data", administrationService.getRegistrations());
    return Tiles.Administration.REGISTRATIONS;
  }

  @RequestMapping(value = {"/administration/registration/paid/{id}"}, method = RequestMethod.GET)
  public String flagAsPaid(@PathVariable int id,ModelMap model, RedirectAttributes redirectAttributes) {
    administrationService.flagAsPaid(id);

    UriComponents uri = ServletUriComponentsBuilder
        .fromPath(Administration.REGISTRATIONS)
        .build();
    redirectAttributes.addFlashAttribute("success", true);
    return "redirect:" + uri.toUriString();
  }

  @RequestMapping(value = {"/administration/users/resetPassword/{id}"}, method = RequestMethod.GET)
  public String resetPassword(@PathVariable Long id,ModelMap model, RedirectAttributes redirectAttributes) {
    changePasswordService.resetPassword(id);
    UriComponents uri = ServletUriComponentsBuilder
        .fromPath(Administration.USERS)
        .build();
    redirectAttributes.addFlashAttribute("success", true);
    return "redirect:" + uri.toUriString();
  }

  @RequestMapping(value = "/administration/activities/export", method = RequestMethod.GET)
  public void getExcel(final HttpServletResponse response) throws IOException {
    response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    response.addHeader("Content-Disposition", "attachment; filename=" + "export.xlsx");
    try {
      exporterService.exportActivities().write(response.getOutputStream());
      response.getOutputStream().flush();
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }
}

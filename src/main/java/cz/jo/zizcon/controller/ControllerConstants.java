package cz.jo.zizcon.controller;

/**
 * Created by User on 3.6.2017.
 */
public interface ControllerConstants {

  public interface Tiles {

    public static final String E404 = "error404";


    public static final String HOMEPAGE = "homepage";
    public static final String REGISTRATION = "registration";
    public static final String LEGAL = "legal";
    public static final String RULES = "rules";
    public static final String CONTACTS = "contacts";
    public static final String LOGIN = "login";
    public static final String FORGOTTEN_PASSWORD = "forgotten-password";

    public static final String ABOUTUS_ANOTHER_ACTIVITIES = "aboutus.anotherActivities";
    public static final String ABOUTUS_NEWS = "aboutus.news";
    public static final String ABOUTUS_NEWS_DETAIL = "aboutus.news.detail";
    public static final String ABOUTUS_TEAM = "aboutus.team";
    public static final String ABOUTUS_WHO_ARE_WE = "aboutus.whoarewe";

    public static final String PROGRAM_LIST = "program.list";
    public static final String PROGRAM_TABLE = "program.table";
    public static final String PROGRAM_DETAIL = "program.detail";

    public interface Administration {
      public static final String HOME = "administration.home";
      public static final String USERS = "administration.users";
      public static final String ACTIVITIES = "administration.activities";
      public static final String REGISTRATIONS = "administration.registrations";
    }

    public interface Profile {
      public static final String HOME = "profile.home";
      public static final String ACTIVITIES = "profile.my-activities";
    }

  }

  public interface Mapping {


    public static final String HOME = "/";
    public static final String LEGAL = "/legal";
    public static final String REGISTRATION = "/registration";
    public static final String RULES = "/rules";
    public static final String CONTACTS = "/contacts";
    public static final String LOGIN = "/login";
    public static final String FORGOTTEN_PASSWORD = "/forgottenPassword";

    public static final String ABOUTUS_ANOTHER_ACTIVITIES = "/aboutus/anotherActivities";
    public static final String ABOUTUS_NEWS = "/aboutus/news";
    public static final String ABOUTUS_NEWS_DETAIL = "/aboutus/news/detail/{id}";
    public static final String ABOUTUS_TEAM = "/aboutus/team";
    public static final String ABOUTUS_WHO_ARE_WE = "/aboutus/whoarewe";

    public static final String PROGRAM_LIST = "/program/list";
    public static final String PROGRAM_TABLE = "/program/table";
    public static final String PROGRAM_DETAIL = "/program/detail/{id}";

    public interface Administration {
      public static final String HOME = "/administration";
      public static final String USERS = "/administration/users";
      public static final String ACTIVITIES = "/administration/activities";
      public static final String REGISTRATIONS = "/administration/registrations";
    }

    public interface Profile {
      public static final String HOME = "/profile";
      public static final String ACTIVITIES = "/profile/my-activities";
    }

  }

}

package cz.jo.zizcon.controller.profile;

import cz.jo.zizcon.controller.ControllerConstants.Mapping.Profile;
import cz.jo.zizcon.controller.ControllerConstants.Tiles;
import cz.jo.zizcon.exceptions.PasswordNotCorrectException;
import cz.jo.zizcon.model.ZizconUserPrincipal;
import cz.jo.zizcon.model.dto.UserDTO;
import cz.jo.zizcon.model.dto.UserRegistrationDTO;
import cz.jo.zizcon.service.ChangePasswordService;
import cz.jo.zizcon.service.ProgramService;
import cz.jo.zizcon.service.UserService;
import java.security.Principal;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ProfileController {

  @Autowired
  private UserService userService;

  @Autowired
  private ProgramService programService;

  @Autowired
  private ChangePasswordService changePasswordService;

  @RequestMapping(value = {Profile.HOME}, method = RequestMethod.GET)
  public String profileHome(ModelMap model, Principal principal) {
    UserDTO user = new UserDTO();
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (!(auth instanceof AnonymousAuthenticationToken)) {
      ZizconUserPrincipal userDetails =
          (ZizconUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
      user =userService.findUserByUserNameDto(userDetails.getUsername());
    }
    model.addAttribute("user", user);
    return Tiles.Profile.HOME;
  }

  @RequestMapping(value = {"/user/action/changePassword"}, method = RequestMethod.POST)
  public String profileHome(ModelMap model,@Validated @ModelAttribute(name = "user") UserDTO userDto,
      BindingResult bindingResult) {
    if (!bindingResult.hasErrors()) {
      try {
        changePasswordService.changePassword(userDto);
        return Tiles.Profile.HOME;
      } catch (PasswordNotCorrectException e) {
        bindingResult.rejectValue("passwordAgain", "user.passwords.notmatch");
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return Tiles.Profile.HOME;
  }



  @RequestMapping(value = {Profile.ACTIVITIES}, method = RequestMethod.GET)
  public String profileMyActivities(ModelMap model,Locale locale) {
    model.addAttribute("activities",  programService.getAll(new PageRequest(0,1000), locale,true).getContent());
    return Tiles.Profile.ACTIVITIES;
  }
}

package cz.jo.zizcon.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 6.6.2017.
 */
@Controller
public class ErrorController implements org.springframework.boot.autoconfigure.web.ErrorController {

  private static final String PATH = "/error";

  @RequestMapping(value = PATH)
  public ModelAndView renderErrorPage(HttpServletRequest httpRequest) {

    ModelAndView errorPage = new ModelAndView(ControllerConstants.Tiles.E404);
    String errorMsg = "";
    int httpErrorCode = getErrorCode(httpRequest);

    switch (httpErrorCode) {
      case 400: {
        errorMsg = "Http Error Code: 400. Bad Request";
        break;
      }
      case 401: {
        errorMsg = "Http Error Code: 401. Unauthorized";
        break;
      }
      case 404: {
        errorMsg = "Http Error Code: 404. Resource not found";
        break;
      }
      case 500: {
        errorMsg = "Http Error Code: 500. Internal Server Error";
        break;
      }
    }
    errorPage.addObject("errorMsg", errorMsg);
    return errorPage;
  }

  private int getErrorCode(HttpServletRequest httpRequest) {
    return (Integer) httpRequest
        .getAttribute("javax.servlet.error.status_code");
  }

  @Override
  public String getErrorPath() {
    return PATH;
  }
}
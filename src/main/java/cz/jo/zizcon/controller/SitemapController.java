package cz.jo.zizcon.controller;

import cz.jo.zizcon.controller.xml.XmlUrl;
import cz.jo.zizcon.controller.xml.XmlUrlSet;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class SitemapController {

  @RequestMapping(value = "/sitemap.xml", method = RequestMethod.GET)
  @ResponseBody
  public XmlUrlSet main() {
    XmlUrlSet xmlUrlSet = new XmlUrlSet();
    create(xmlUrlSet, "", XmlUrl.Priority.HIGH);
    create(xmlUrlSet, "/registration", XmlUrl.Priority.HIGH);
    create(xmlUrlSet, "/link-2", XmlUrl.Priority.MEDIUM);

    return xmlUrlSet;
  }

  //TODO load all pages dynamically
  private void create(XmlUrlSet xmlUrlSet, String link, XmlUrl.Priority priority) {
    xmlUrlSet.addUrl(new XmlUrl("http://www.zizcon.cz" + link, priority));
    xmlUrlSet.addUrl(new XmlUrl("http://zizcon.cz" + link, priority));
  }

}

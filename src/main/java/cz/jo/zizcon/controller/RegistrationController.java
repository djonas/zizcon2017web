package cz.jo.zizcon.controller;

import cz.jo.zizcon.exceptions.EmailAlreadyRegisteredException;
import cz.jo.zizcon.model.dto.UserRegistrationDTO;
import cz.jo.zizcon.service.RegistrationService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

/**
 * Created by User on 3.6.2017.
 */
@Controller
public class RegistrationController {

  @Autowired
  RegistrationService registrationService;

  @Autowired
  MessageSource messageSource;


  public static final String REGISTRATION_DTO = "userRegistration";

  @RequestMapping(value = {ControllerConstants.Mapping.REGISTRATION, "/listky"}, method = RequestMethod.GET)
  public String homePage(Model model, Locale locale) {
    model.addAttribute(REGISTRATION_DTO, new UserRegistrationDTO());
    populateDefaultModel(model);
    return ControllerConstants.Tiles.REGISTRATION;
  }

  @RequestMapping(value = {ControllerConstants.Mapping.REGISTRATION}, method = RequestMethod.POST)
  public String doRegister(Model model, @Validated @ModelAttribute(name = REGISTRATION_DTO) UserRegistrationDTO userRegistration,
      BindingResult bindingResult) {
    if (!bindingResult.hasErrors()) {
      try {
        userRegistration = registrationService.save(userRegistration);
        model.addAttribute("success", userRegistration);
        return ControllerConstants.Tiles.REGISTRATION;
      } catch (EmailAlreadyRegisteredException e) {
        bindingResult.rejectValue("email", "registration.form.error.alreadyExists");
      } catch (Exception e) {
        e.printStackTrace();
      }
    } else {

    }
    populateDefaultModel(model);
    return ControllerConstants.Tiles.REGISTRATION;
  }


  public void populateDefaultModel(Model model) {
    model.addAttribute("tickets", registrationService.getAllTicketTypes());
  }

}

package cz.jo.zizcon.controller;

import cz.jo.zizcon.controller.util.PageWrapper;
import cz.jo.zizcon.model.dto.ActivityDTO;
import cz.jo.zizcon.model.dto.AttendanceChangeDTO;
import cz.jo.zizcon.model.entity.Activity;
import cz.jo.zizcon.service.ProgramService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContext;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Locale;
import java.util.Optional;

@Controller
public class ProgramController {

  @Autowired
  ProgramService programService;

  @Autowired
  private MessageSource messageSource;


  @RequestMapping(value = {ControllerConstants.Mapping.PROGRAM_LIST}, method = RequestMethod.GET)
  public String list(@RequestParam(value = "type", defaultValue = "") String type,
                     @RequestParam(value="myActivities", defaultValue = "false") boolean myActivities,
      Model model, Locale locale, @PageableDefault(sort = {"fromDate"}, value = 10) Pageable pageable) {
    Page<ActivityDTO> activities;
    if (type == null || "".equals(type)) {
      activities = programService.getAll(pageable, locale, myActivities);

    } else {
      activities = programService.getForType(type, locale,myActivities, pageable);
    }
    PageWrapper<ActivityDTO> page = new PageWrapper<>(activities, ControllerConstants.Mapping.PROGRAM_LIST);
    page.setParams("type="+type+"&myActivities="+myActivities);
    model.addAttribute("type", type);
    model.addAttribute("myActivities", myActivities);
    model.addAttribute("activities", page.getContent());
    model.addAttribute("page", page);
    model.addAttribute("view","list");
    model.addAttribute("types",createTypesList());
    addCommomModel(model);
    return ControllerConstants.Tiles.PROGRAM_LIST;
  }

  @RequestMapping(value = {ControllerConstants.Mapping.PROGRAM_TABLE}, method = RequestMethod.GET)
  public String table(@RequestParam(value = "type", defaultValue = "") String type,
      @RequestParam(value="myActivities", defaultValue = "false") boolean myActivities,
      Model model, Locale locale) {
    model.addAttribute("type", type);
    model.addAttribute("locale", locale);
    model.addAttribute("myActivities", myActivities);
    model.addAttribute("view","table");
    addCommomModel(model);
    return ControllerConstants.Tiles.PROGRAM_TABLE;
  }


  @RequestMapping(value = {ControllerConstants.Mapping.PROGRAM_DETAIL}, method = RequestMethod.GET)
  public String detail(@PathVariable("id") String id, Model model, Locale locale, RedirectAttributes redirectAttributes) {

    Optional<ActivityDTO> activityDetailDTO = programService.getDetail(id, locale);

    if (id == null || "".equals(id) || !activityDetailDTO.isPresent()) {
      redirectAttributes.addFlashAttribute("error", "program.list.nonexistentActivity");
      return "redirect:" + ControllerConstants.Tiles.PROGRAM_LIST;
    } else {

      model.addAttribute("activity", activityDetailDTO.get());
      model.addAttribute("specificTitle", activityDetailDTO.get().getTitle());
      addCommomModel(model);
      return ControllerConstants.Tiles.PROGRAM_DETAIL;
    }
  }

  private void addCommomModel(Model model) {



  }

  /* --- JSON API --- */

  @RequestMapping(value="/api/getSchedule", method = RequestMethod.GET)
  public @ResponseBody List<ActivityDTO> getActivitiesJson(@RequestParam(value = "type", defaultValue = "") String type,
      @RequestParam(value="myActivities", defaultValue = "false") boolean myActivities,
      Model model, Locale locale) {
    Pageable p = new PageRequest(0, 1000);
    Page<ActivityDTO> activities;
    if (type == null || "".equals(type)) {
      activities = programService.getAll(p, locale, myActivities);

    } else {
      activities = programService.getForType(type, locale,myActivities, p);
    }
    return activities.getContent();
  }

  @RequestMapping(value="/user/action/attend", method = RequestMethod.GET)
  public @ResponseBody
  AttendanceChangeDTO attend(@RequestParam(value = "activityId", defaultValue = "") String activityId,
      Model model) {
    return translateMessage(programService.attend(activityId));
  }

  @RequestMapping(value="/user/action/cancelAttendance", method = RequestMethod.GET)
  public @ResponseBody
  AttendanceChangeDTO cancelAttendance(@RequestParam(value = "activityId", defaultValue = "") String activityId,
      Model model) {
    return translateMessage(programService.cancelAttendance(activityId));
  }

  private AttendanceChangeDTO translateMessage(AttendanceChangeDTO attendanceChangeDTO) {
    Locale locale = LocaleContextHolder.getLocale();
    String message = null;
    try {
      message = messageSource.getMessage(attendanceChangeDTO.getMessage(),null, locale);
    } catch (NoSuchMessageException e) {
      message = "OK";
    }
    if(StringUtils.isNotEmpty(message)){
      attendanceChangeDTO.setMessage(message);
    }
    return attendanceChangeDTO;
  }


  private List<String> createTypesList(){
    List<String> types = new ArrayList<>();
    for(Activity.Type type : Activity.Type.values()){
      types.add(type.getName());
    }
    return types;
  }

}

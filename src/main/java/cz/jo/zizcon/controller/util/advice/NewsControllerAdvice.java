package cz.jo.zizcon.controller.util.advice;

import cz.jo.zizcon.model.dto.NewsDTO;
import cz.jo.zizcon.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;
import java.util.Locale;

/**
 * Created by User on 4.6.2017.
 */
@ControllerAdvice("cz.jo.zizcon.controller")
public class NewsControllerAdvice {

  @Autowired
  private NewsService newsService;


  @ModelAttribute("footerNews")
  public List<NewsDTO> getNews(Locale locale) {
    return newsService.getNewsForFooter(locale);
  }
}

package cz.jo.zizcon.controller.util.formatter;

import cz.jo.zizcon.model.dto.TicketDTO;
import cz.jo.zizcon.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

/**
 * Created by User on 3.6.2017.
 */
public class TicketFormatter implements Formatter<TicketDTO> {

  @Autowired
  private RegistrationService registrationService;

  @Override
  public TicketDTO parse(String s, Locale locale) throws ParseException {
    if (s.equals("NONE")) {
      return new TicketDTO();
    }
    return registrationService.getTicketByCode(s);
  }

  @Override
  public String print(TicketDTO ticketDTO, Locale locale) {
    return ticketDTO.getCode();
  }
}

package cz.jo.zizcon.exceptions;

/**
 * Created by User on 3.6.2017.
 */
public class EmailAlreadyRegisteredException extends RuntimeException {

  public static final String DEFAULT_MESSAGE = "Email is already registered!";

  public EmailAlreadyRegisteredException() {
    super(DEFAULT_MESSAGE);
  }

  public EmailAlreadyRegisteredException(String message) {
    super(message);
  }

}

package cz.jo.zizcon.exceptions;

/**
 * Created by User on 3.6.2017.
 */
public class PasswordNotCorrectException extends RuntimeException {

  public static final String DEFAULT_MESSAGE = "Passwords does not match!";

  public PasswordNotCorrectException() {
    super(DEFAULT_MESSAGE);
  }

  public PasswordNotCorrectException(String message) {
    super(message);
  }

}

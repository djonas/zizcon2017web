package cz.jo.zizcon.model.entity;

import cz.jo.zizcon.model.dto.NewsDTO;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by User on 5.6.2017.
 */

@Entity
@Table(name = "NEWS")
public class News {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "newsIdGenerator")
  @SequenceGenerator(name = "newsIdGenerator", sequenceName = "SEQ_ID_NEWS")
  @Column(unique = true, nullable = false)
  private Integer id;

  @Column(nullable = false)
  private String summary;

  @Column(nullable = false)
  private String title;

  @Column(nullable = false)
  private String language;

  @Column(nullable = false)
  private Date published;

  private String author;

  @Lob
  private String fullTextation;

  public News() {
  }

  public News(String summary, String title, Date published, String author, String fullTextation) {
    this.summary = summary;
    this.title = title;
    this.published = published;
    this.author = author;
    this.fullTextation = fullTextation;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Date getPublished() {
    return published;
  }

  public void setPublished(Date published) {
    this.published = published;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public News(String summary) {
    this.summary = summary;
  }

  public String getFullTextation() {
    return fullTextation;
  }

  public void setFullTextation(String fullTextation) {
    this.fullTextation = fullTextation;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public static Builder getBuilder(NewsDTO newsDto) {
    return new Builder(newsDto);
  }

  public static class Builder {

    private News build;

    Builder(NewsDTO newsDto) {
      build = new News();
      build.setAuthor(newsDto.getAuthor());
      build.setFullTextation(newsDto.getFullTextation());
      build.setId(newsDto.getId());
      build.setPublished(newsDto.getPublished());
      build.setSummary(newsDto.getSummary());
      build.setTitle(newsDto.getTitle());
      build.setLanguage(newsDto.getLanguage());
    }

    public News build() {
      return build;
    }
  }
}

package cz.jo.zizcon.model.entity;


import cz.jo.zizcon.model.dto.ActivityDTO;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by UserRegistration on 25.5.2017.
 */
@Entity
public class Activity implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "activityIdGenerator")
  @SequenceGenerator(name = "activityIdGenerator", sequenceName = "SEQ_ID_NEWS")
  @Column(unique = true, nullable = false)
  private Integer id;

  @Column(unique = true, nullable = false)
  private String stringId;


  private Date added;
  private String organizer;


  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private Type activityType;

  private Date fromDate;
  private Date toDate;


  @Lob
  private byte[] imageBig;

  @Lob
  private byte[] imageThumb;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "activity")
  private List<ActivityTexts> textation;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "activity")
  private List<ActivityAttendingLimit> limits;

  @OneToMany(mappedBy = "activity")
  private Set<ActivityAttendee> attendees;

  public Activity() {
  }

  public Activity(String stringId, Date added, String organizer, Type activityType, Date fromDate, Date toDate, int minAttendees, int maxAttendees, byte[] imageBig, byte[] imageThumb,
      List<ActivityTexts> textation) {
    this.stringId = stringId;
    this.added = added;
    this.organizer = organizer;
    this.activityType = activityType;
    this.fromDate = fromDate;
    this.toDate = toDate;
    this.imageBig = imageBig;
    this.imageThumb = imageThumb;
    this.textation = textation;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }


  public Date getAdded() {
    return added;
  }

  public void setAdded(Date added) {
    this.added = added;
  }

  public String getOrganizer() {
    return organizer;
  }

  public void setOrganizer(String organizer) {
    this.organizer = organizer;
  }


  public Type getActivityType() {
    return activityType;
  }

  public void setActivityType(Type activityType) {
    this.activityType = activityType;
  }

  public Date getFromDate() {
    return fromDate;
  }

  public void setFromDate(Date from) {
    this.fromDate = fromDate;
  }

  public Date getToDate() {
    return toDate;
  }

  public void setToDate(Date toDate) {
    this.toDate = toDate;
  }

  public String getStringId() {
    return stringId;
  }

  public void setStringId(String stringId) {
    this.stringId = stringId;
  }

  public byte[] getImageBig() {
    return imageBig;
  }

  public void setImageBig(byte[] imageBig) {
    this.imageBig = imageBig;
  }

  public byte[] getImageThumb() {
    return imageThumb;
  }

  public void setImageThumb(byte[] imageThumb) {
    this.imageThumb = imageThumb;
  }

  public List<ActivityAttendingLimit> getLimits() {
    return limits;
  }

  public void setLimits(List<ActivityAttendingLimit> limits) {
    this.limits = limits;
  }

  public Collection<ActivityAttendee> getAttendees() {
    return attendees;
  }

  public void setAttendees(Set<ActivityAttendee> attendees) {
    this.attendees = attendees;
  }

  public static Builder getBuilder(ActivityDTO activityDto) {
    return new Builder(activityDto);
  }

  public List<ActivityTexts> getTextation() {
    return textation;
  }

  public void setTextation(List<ActivityTexts> textation) {
    this.textation = textation;
  }

  public static class Builder {

    private Activity build;

    Builder(ActivityDTO activityDTO) {
      build = new Activity();
      build.setAdded(activityDTO.getAdded());
      build.setFromDate(activityDTO.getFrom());
      build.setToDate(activityDTO.getTo());
      build.setId(activityDTO.getId());
      build.setActivityType(activityDTO.getType());

      build.setOrganizer(activityDTO.getOrganizer());
      build.setStringId(activityDTO.getStringId());
    }

    public Activity build() {
      return build;
    }
  }

  public enum Type {
    BOARD_GAME("BG"),
    RPG("RPG"),
    LARP("LARP"),
    OTHER("OTH");

    private String type;

    Type(String type) {
      this.type = type;
    }

    public String getName() {
      return type;
    }

    public static Type getByName(String name) {
      for (Type type : Type.values()) {
        if (type.getName().equals(name)) {
          return type;
        }
      }
      return null;
    }

  }
}

package cz.jo.zizcon.model.entity;


import cz.jo.zizcon.model.dto.TicketDTO;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * Created by User on 25.5.2017.
 */
@Entity
@Table(name = "TICKET", uniqueConstraints = {@UniqueConstraint(columnNames = {"ID", "CODE"})})
public class Ticket implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "ticketIdGenerator")
  @SequenceGenerator(name = "ticketIdGenerator", sequenceName = "SEQ_ID_TICKET")
  @Column(unique = true, nullable = false)
  private Integer id;
  @Column(nullable = false)
  private String name;
  @Column(nullable = false)
  private String code;
  @Column(nullable = false)
  private int price;

  private Date validFrom;

  private Date validTo;

  private Date registrableFrom;

  private Date registrableTo;

  public Ticket() {

  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public Date getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(Date validFrom) {
    this.validFrom = validFrom;
  }

  public Date getValidTo() {
    return validTo;
  }

  public void setValidTo(Date validTo) {
    this.validTo = validTo;
  }

  public Date getRegistrableFrom() {
    return registrableFrom;
  }

  public void setRegistrableFrom(Date registrableFrom) {
    this.registrableFrom = registrableFrom;
  }

  public Date getRegistrableTo() {
    return registrableTo;
  }

  public void setRegistrableTo(Date registrableTo) {
    this.registrableTo = registrableTo;
  }

  public Ticket(String name, String code, int price, Date validFrom, Date validTo,
      Date registrableFrom, Date registrableTo) {
    this.name = name;
    this.code = code;
    this.price = price;
    this.validFrom = validFrom;
    this.validTo = validTo;
    this.registrableFrom = registrableFrom;
    this.registrableTo = registrableTo;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Ticket)) {
      return false;
    }

    Ticket ticket = (Ticket) o;

    if (price != ticket.price) {
      return false;
    }
    if (!name.equals(ticket.name)) {
      return false;
    }
    if (!code.equals(ticket.code)) {
      return false;
    }
    if (validFrom != null ? !validFrom.equals(ticket.validFrom) : ticket.validFrom != null) {
      return false;
    }
    if (validTo != null ? !validTo.equals(ticket.validTo) : ticket.validTo != null) {
      return false;
    }
    if (registrableFrom != null ? !registrableFrom.equals(ticket.registrableFrom)
        : ticket.registrableFrom != null) {
      return false;
    }
    return registrableTo != null ? registrableTo.equals(ticket.registrableTo)
        : ticket.registrableTo == null;
  }

  @Override
  public int hashCode() {
    int result = name.hashCode();
    result = 31 * result + code.hashCode();
    result = 31 * result + price;
    result = 31 * result + (validFrom != null ? validFrom.hashCode() : 0);
    result = 31 * result + (validTo != null ? validTo.hashCode() : 0);
    result = 31 * result + (registrableFrom != null ? registrableFrom.hashCode() : 0);
    result = 31 * result + (registrableTo != null ? registrableTo.hashCode() : 0);
    return result;
  }

  public static Builder getBuilder(TicketDTO ticketDTO) {
    return new Builder(ticketDTO);
  }

  public static class Builder {

    private Ticket build;

    Builder(TicketDTO ticketDTO) {
      build = new Ticket();
      build.setId(ticketDTO.getId());
      build.setCode(ticketDTO.getCode());
      build.setName(ticketDTO.getName());
      build.setPrice(ticketDTO.getPrice());
      build.setValidFrom(ticketDTO.getValidFrom());
      build.setValidTo(ticketDTO.getValidTo());
    }

    public Ticket build() {
      return build;
    }
  }


}

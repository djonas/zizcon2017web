package cz.jo.zizcon.model.entity;

import cz.jo.zizcon.model.dto.UserRegistrationDTO;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"email"})})
public class UserRegistration {


  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "userRegistrationIdGenerator")
  @SequenceGenerator(name = "userRegistrationIdGenerator", sequenceName = "SEQ_ID_USER_REGISTRATION")
  private Integer id;

  @Column(nullable = false)
  private String email;

  @Column(nullable = false)
  private String firstName;

  @Column(nullable = false)
  private String surname;

  private String nickname;
  private String phoneNumber;
  private String gender;
  private String street;
  private String city;
  private String zipCode;

  @Column(nullable = false)
  private String yearOfBirth;

  @OneToOne
  @JoinColumn(name = "TICKET_CODE", nullable = false, referencedColumnName = "code")
  private Ticket ticket;

  private Date created;

  private boolean paid;

  public UserRegistration() {
    this.ticket = new Ticket();
  }

  public Integer getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getYearOfBirth() {
    return yearOfBirth;
  }

  public void setYearOfBirth(String yearOfBirth) {
    this.yearOfBirth = yearOfBirth;
  }

  public Ticket getTicket() {
    return ticket;
  }

  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public boolean isPaid() {
    return paid;
  }

  public void setPaid(boolean paid) {
    this.paid = paid;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  public UserRegistration(String email, String firstName, String surname, String nickname,
      String phoneNumber, String gender, String street, String city, String zipCode,
      String yearOfBirth, Ticket ticket) {
    this.email = email;
    this.firstName = firstName;
    this.surname = surname;
    this.nickname = nickname;
    this.phoneNumber = phoneNumber;
    this.gender = gender;
    this.street = street;
    this.city = city;
    this.zipCode = zipCode;
    this.yearOfBirth = yearOfBirth;
    this.ticket = ticket;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof UserRegistration)) {
      return false;
    }

    UserRegistration that = (UserRegistration) o;

    if (yearOfBirth != that.yearOfBirth) {
      return false;
    }
    if (!email.equals(that.email)) {
      return false;
    }
    if (!firstName.equals(that.firstName)) {
      return false;
    }
    if (!surname.equals(that.surname)) {
      return false;
    }
    if (nickname != null ? !nickname.equals(that.nickname) : that.nickname != null) {
      return false;
    }
    if (phoneNumber != null ? !phoneNumber.equals(that.phoneNumber) : that.phoneNumber != null) {
      return false;
    }
    if (gender != null ? !gender.equals(that.gender) : that.gender != null) {
      return false;
    }
    if (street != null ? !street.equals(that.street) : that.street != null) {
      return false;
    }
    if (city != null ? !city.equals(that.city) : that.city != null) {
      return false;
    }
    if (zipCode != null ? !zipCode.equals(that.zipCode) : that.zipCode != null) {
      return false;
    }
    return ticket != null ? ticket.equals(that.ticket) : that.ticket == null;
  }

  @Override
  public int hashCode() {
    int result = email.hashCode();
    result = 31 * result + firstName.hashCode();
    result = 31 * result + surname.hashCode();
    result = 31 * result + (nickname != null ? nickname.hashCode() : 0);
    result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
    result = 31 * result + (gender != null ? gender.hashCode() : 0);
    result = 31 * result + (street != null ? street.hashCode() : 0);
    result = 31 * result + (city != null ? city.hashCode() : 0);
    result = 31 * result + (zipCode != null ? zipCode.hashCode() : 0);
    result = 31 * result + (yearOfBirth != null ? yearOfBirth.hashCode() : 0);
    ;
    result = 31 * result + (ticket != null ? ticket.hashCode() : 0);
    return result;
  }

  public static Builder getBuilder(UserRegistrationDTO userRegDto) {
    return new Builder(userRegDto);
  }

  public static class Builder {

    private UserRegistration build;

    Builder(UserRegistrationDTO userRegistrationDTO) {
      build = new UserRegistration();
      build.setCity(userRegistrationDTO.getCity());
      build.setEmail(userRegistrationDTO.getEmail());
      build.setFirstName(userRegistrationDTO.getFirstName());
      build.setGender(userRegistrationDTO.getGender());
      build.setNickname(userRegistrationDTO.getNickname());
      build.setPhoneNumber(userRegistrationDTO.getPhoneNumber());
      build.setStreet(userRegistrationDTO.getStreet());
      build.setSurname(userRegistrationDTO.getSurname());
      build.setTicket(Ticket.getBuilder(userRegistrationDTO.getTicket()).build());
      build.setYearOfBirth(userRegistrationDTO.getYearOfBirth());
      build.setZipCode(userRegistrationDTO.getZipCode());
      build.setPaid(userRegistrationDTO.isPaid());
      build.setCreated(userRegistrationDTO.getCreated());
    }

    public UserRegistration build() {
      return build;
    }
  }


}

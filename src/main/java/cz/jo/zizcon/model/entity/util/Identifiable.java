package cz.jo.zizcon.model.entity.util;

import java.io.Serializable;

/**
 * Created by User on 4.7.2017.
 */
public interface Identifiable<T extends Serializable> {

  T getId();

}

package cz.jo.zizcon.model.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class ActivityAttendee implements Serializable {

  @Id
  @GeneratedValue
  @Column(name = "ATTENDEE_ID")
  long id;

  @ManyToOne
  @JoinColumn(name = "activity_id", referencedColumnName = "stringId")
  private Activity activity;

  @ManyToOne
  @JoinColumn(name = "user_id", referencedColumnName = "username")
  private User user;

  private Date timeAttended;

  public Activity getActivity() {
    return activity;
  }

  public void setActivity(Activity activity) {
    this.activity = activity;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Date getTimeAttended() {
    return timeAttended;
  }

  public void setTimeAttended(Date timeAttended) {
    this.timeAttended = timeAttended;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    ActivityAttendee that = (ActivityAttendee) o;

    if (id != that.id) {
      return false;
    }
    if (activity != null ? !activity.equals(that.activity) : that.activity != null) {
      return false;
    }
    if (user != null ? !user.equals(that.user) : that.user != null) {
      return false;
    }
    return timeAttended != null ? timeAttended.equals(that.timeAttended) : that.timeAttended == null;
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (activity != null ? activity.hashCode() : 0);
    result = 31 * result + (user != null ? user.hashCode() : 0);
    result = 31 * result + (timeAttended != null ? timeAttended.hashCode() : 0);
    return result;
  }
}
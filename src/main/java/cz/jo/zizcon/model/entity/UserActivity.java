package cz.jo.zizcon.model.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by User on 20.6.2017.
 */
//TODO
//@Entity
//@Table(name = "user_activity")
public class UserActivity {

  @Id
  @ManyToOne
  @JoinColumn(name = "USER_ID", nullable = false)
  private User user;

  @Id
  @ManyToOne
  @JoinColumn(name = "ACTIVITY_ID", nullable = false)
  private Activity activity;

  private Date registered;

  @Enumerated(EnumType.STRING)
  private State registrationState;


  public enum State {
    WAITING_FOR_APPROVAL(0),
    APPROVED(1),
    RESERVED(2);

    private int numVal;

    State(int numVal) {
      this.numVal = numVal;
    }

    public int getNumVal() {
      return numVal;
    }
  }

}

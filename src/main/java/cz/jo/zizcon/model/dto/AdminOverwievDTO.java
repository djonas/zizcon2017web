package cz.jo.zizcon.model.dto;

public class AdminOverwievDTO {

  private int numOfRegistered;
  private int numOfPaidRegistrations;
  private int numOfActivities;
  private int numOfFullActivities;

  public int getNumOfRegistered() {
    return numOfRegistered;
  }

  public void setNumOfRegistered(int numOfRegistered) {
    this.numOfRegistered = numOfRegistered;
  }

  public int getNumOfPaidRegistrations() {
    return numOfPaidRegistrations;
  }

  public void setNumOfPaidRegistrations(int numOfPaidRegistrations) {
    this.numOfPaidRegistrations = numOfPaidRegistrations;
  }

  public int getNumOfActivities() {
    return numOfActivities;
  }

  public void setNumOfActivities(int numOfActivities) {
    this.numOfActivities = numOfActivities;
  }

}

package cz.jo.zizcon.model.dto;

public class ActivityFilterDTO {

  String type;
  boolean myActivities;

  public ActivityFilterDTO() {
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public boolean isMyActivities() {
    return myActivities;
  }

  public void setMyActivities(boolean myActivities) {
    this.myActivities = myActivities;
  }
}

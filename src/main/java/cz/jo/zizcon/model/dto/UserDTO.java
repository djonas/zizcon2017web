package cz.jo.zizcon.model.dto;


import cz.jo.zizcon.model.entity.Ticket;
import cz.jo.zizcon.model.entity.User;
import javax.validation.constraints.NotNull;

public class UserDTO {

  @NotNull
  private Long id;
  private String email;
  private String firstName;
  private String surname;
  private String username;
  private String phoneNumber;
  private String gender;
  private String street;
  private String city;
  private String zipCode;
  private int yearOfBirth;
  @NotNull
  private String password;
  @NotNull
  private String passwordAgain;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public int getYearOfBirth() {
    return yearOfBirth;
  }

  public void setYearOfBirth(int yearOfBirth) {
    this.yearOfBirth = yearOfBirth;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getPasswordAgain() {
    return passwordAgain;
  }

  public void setPasswordAgain(String passwordAgain) {
    this.passwordAgain = passwordAgain;
  }
  public static Builder getBuilder(User user) {
    return new Builder(user);
  }

  public static class Builder {

    private UserDTO build;

    Builder(User user) {
      build = new UserDTO();
      build.setId(user.getId());
      build.setCity(user.getCity());
      build.setEmail(user.getEmail());
      build.setFirstName(user.getFirstName());
      build.setSurname(user.getSurname());
      build.setGender(user.getGender());
      build.setPhoneNumber(user.getPhoneNumber());
      build.setStreet(user.getStreet());
      build.setYearOfBirth(user.getYearOfBirth());
      build.setZipCode(user.getZipCode());
      build.setUsername(user.getUsername());
    }

    public UserDTO build() {
      return build;
    }
  }
}

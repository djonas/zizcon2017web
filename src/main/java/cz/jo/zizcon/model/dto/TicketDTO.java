package cz.jo.zizcon.model.dto;

import cz.jo.zizcon.model.entity.Ticket;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by User on 3.6.2017.
 */
public class TicketDTO {

  private Integer id;
  private String name;
  private String code;
  private int price;
  Date validFrom;
  Date validTo;

  public TicketDTO() {
  }

  public TicketDTO(Integer id, String name, String code, int price, Date validFrom, Date validTo) {
    this.id = id;
    this.name = name;
    this.code = code;
    this.price = price;
    this.validFrom = validFrom;
    this.validTo = validTo;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public Date getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(Date validFrom) {
    this.validFrom = validFrom;
  }

  public Date getValidTo() {
    return validTo;
  }

  public void setValidTo(Date validTo) {
    this.validTo = validTo;
  }

  public static Builder getBuilder(Ticket ticket) {
    return new Builder(ticket);
  }

  public static class Builder {

    private TicketDTO build;

    Builder(Ticket ticket) {
      build = new TicketDTO();
      build.setCode(ticket.getCode());
      build.setId(ticket.getId());
      build.setName(ticket.getName());
      build.setPrice(ticket.getPrice());
      build.setValidFrom(ticket.getValidFrom());
      build.setValidTo(ticket.getValidTo());
    }

    public TicketDTO build() {
      return build;
    }
  }


}

package cz.jo.zizcon.model.dto;

import cz.jo.zizcon.model.entity.Activity;

import cz.jo.zizcon.model.entity.ActivityAttendingLimit;
import cz.jo.zizcon.model.entity.ActivityTexts;
import cz.jo.zizcon.service.impl.ProgramServiceHelper;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 * Created by User on 6.6.2017.
 */
public class ActivityDTO {

  private Integer id;
  private String stringId;
  private String summary;
  private String title;
  private String language;
  private Date added;
  private String organizer;
  private String description;
  private Activity.Type type;
  private Date from;
  private Date to;

  private int minAttendees;
  private int maxAttendees;
  private int attendees;
  private boolean isAttendable;
  private String reasonNotAttendable;
  Map<String,LimitDTO> limits;

  public ActivityDTO() {
  }


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }


  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public Date getAdded() {
    return added;
  }

  public void setAdded(Date added) {
    this.added = added;
  }

  public String getOrganizer() {
    return organizer;
  }

  public void setOrganizer(String organizer) {
    this.organizer = organizer;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Activity.Type getType() {
    return type;
  }

  public void setType(Activity.Type type) {
    this.type = type;
  }

  public Date getFrom() {
    return from;
  }

  public void setFrom(Date from) {
    this.from = from;
  }

  public Date getTo() {
    return to;
  }

  public void setTo(Date to) {
    this.to = to;
  }

  public String getStringId() {
    return stringId;
  }

  public void setStringId(String stringId) {
    this.stringId = stringId;
  }

  public int getMinAttendees() {
    return minAttendees;
  }

  public void setMinAttendees(int minAttendees) {
    this.minAttendees = minAttendees;
  }

  public int getMaxAttendees() {
    return maxAttendees;
  }

  public void setMaxAttendees(int maxAttendees) {
    this.maxAttendees = maxAttendees;
  }

  public boolean isAttendable() {
    return isAttendable;
  }

  public void setAttendable(boolean attendable) {
    isAttendable = attendable;
  }

  public String getReasonNotAttendable() {
    return reasonNotAttendable;
  }

  public void setReasonNotAttendable(String reasonNotAttendable) {
    this.reasonNotAttendable = reasonNotAttendable;
  }

  public int getAttendees() {
    return attendees;
  }

  public void setAttendees(int attendees) {
    this.attendees = attendees;
  }

  public Map<String, LimitDTO> getLimits() {
    return limits;
  }

  public void setLimits(Map<String, LimitDTO> limits) {
    this.limits = limits;
  }

  public static Builder getBuilder(Activity activity) {
    return new Builder(activity);
  }

  public static class Builder {

    private ActivityDTO build;

    Builder(Activity activity) {
      build = new ActivityDTO();
      build.setAdded(activity.getAdded());
      build.setStringId(activity.getStringId());
      build.setFrom(activity.getFromDate());
      build.setTo(activity.getToDate());
      build.setId(activity.getId());
      build.setType(activity.getActivityType());
      build.setOrganizer(activity.getOrganizer());
      build.setAttendees(activity.getAttendees().size());
      Locale locale = LocaleContextHolder.getLocale();
      for(ActivityTexts activityTexts : activity.getTextation()){
        if(activity.getTextation().size() == 1 ||activityTexts.getLanguage().equals( locale.getLanguage())){
          build.setTitle(activityTexts.getTitle());
          build.setLanguage(activityTexts.getLanguage());
          build.setDescription(activityTexts.getDescription());
          build.setSummary(activityTexts.getSummary());
          break;
        }
      }
      build.setLimits(ProgramServiceHelper.createLimitDtos(activity));
      build.getLimits().forEach((key, value) -> {
        build.setMaxAttendees(build.getMaxAttendees() + value.getMax());
      });
    }

    public ActivityDTO build() {
      return build;
    }
  }

  public static class LimitDTO {
      String name;
      int max;
      int min;
      int used;

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public int getMax() {
      return max;
    }

    public void setMax(int max) {
      this.max = max;
    }

    public int getMin() {
      return min;
    }

    public void setMin(int min) {
      this.min = min;
    }

    public int getUsed() {
      return used;
    }

    public void setUsed(int used) {
      this.used = used;
    }
  }
}

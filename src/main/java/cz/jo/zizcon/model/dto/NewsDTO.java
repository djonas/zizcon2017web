package cz.jo.zizcon.model.dto;

import cz.jo.zizcon.model.entity.News;

import javax.persistence.Column;
import javax.persistence.Lob;
import java.util.Date;

/**
 * Created by User on 4.6.2017.
 */
public class NewsDTO {

  private Integer id;
  private String summary;
  private String title;
  private Date published;
  private String author;
  private String fullTextation;

  private String language;

  public NewsDTO() {
  }

  public NewsDTO(Integer id, String summary, String title, Date published, String author, String fullTextation) {
    this.id = id;
    this.summary = summary;
    this.title = title;
    this.published = published;
    this.author = author;
    this.fullTextation = fullTextation;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Date getPublished() {
    return published;
  }

  public void setPublished(Date published) {
    this.published = published;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getFullTextation() {
    return fullTextation;
  }

  public void setFullTextation(String fullTextation) {
    this.fullTextation = fullTextation;
  }

  public static Builder getBuilder(News news) {
    return new Builder(news);
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public static class Builder {

    private NewsDTO build;

    Builder(News news) {
      build = new NewsDTO();
      build.setAuthor(news.getAuthor());
      build.setFullTextation(news.getFullTextation());
      build.setId(news.getId());
      build.setPublished(news.getPublished());
      build.setSummary(news.getSummary());
      build.setTitle(news.getTitle());
      build.setLanguage(news.getLanguage());
    }

    public NewsDTO build() {
      return build;
    }
  }
}

package cz.jo.zizcon.model.dto;

import cz.jo.zizcon.model.entity.UserRegistration;
import java.util.Calendar;
import java.util.Date;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.validation.constraints.*;

/**
 * Created by User on 3.6.2017.
 */
public class UserRegistrationDTO {


  private Integer id;
  @NotNull
  @NotEmpty
  @Email
  private String email;
  @NotNull
  @NotEmpty
  private String firstName;
  @NotNull
  @NotEmpty
  private String surname;
  private String nickname;
  private String phoneNumber;
  @NotNull
  private String gender;
  private String street;
  private String city;
  private Date created;
  private String zipCode;

  @NotNull
  private String yearOfBirth;
  @NotNull
  private TicketDTO ticket;

  @AssertTrue
  private boolean acceptsTerms;

  private boolean paid;


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public String getYearOfBirth() {
    return yearOfBirth;
  }

  public void setYearOfBirth(String yearOfBirth) {
    this.yearOfBirth = yearOfBirth;
  }

  public TicketDTO getTicket() {
    return ticket;
  }

  public void setTicket(TicketDTO ticket) {
    this.ticket = ticket;
  }

  public boolean isAcceptsTerms() {
    return acceptsTerms;
  }

  public void setAcceptsTerms(boolean acceptsTerms) {
    this.acceptsTerms = acceptsTerms;
  }

  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  public boolean isPaid() {
    return paid;
  }

  public void setPaid(boolean paid) {
    this.paid = paid;
  }

  public boolean isCreatedToday(){
    Calendar c = Calendar.getInstance();

// set the calendar to start of today
    c.set(Calendar.HOUR_OF_DAY, 0);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);
    c.set(Calendar.MILLISECOND, 0);

// and get that as a Date
    Date today = c.getTime();

// or as a timestamp in milliseconds
    long todayInMillis = c.getTimeInMillis();

// user-specified date which you are testing
// let's say the components come from a form or something
    int year = 2011;
    int month = 5;
    int dayOfMonth = 20;

// reuse the calendar to set user specified date
    c.set(Calendar.YEAR, year);
    c.set(Calendar.MONTH, month);
    c.set(Calendar.DAY_OF_MONTH, dayOfMonth);

// and get that as a Date
    Date dateSpecified = c.getTime();
    return !getCreated().before(today);
  }

  public UserRegistrationDTO(Integer id, String email, String firstName, String surname, String nickname, String phoneNumber, String gender, String street, String city, String zipCode,
      String yearOfBirth, TicketDTO ticket) {
    this.id = id;
    this.email = email;
    this.firstName = firstName;
    this.surname = surname;
    this.nickname = nickname;
    this.phoneNumber = phoneNumber;
    this.gender = gender;
    this.street = street;
    this.city = city;
    this.zipCode = zipCode;
    this.yearOfBirth = yearOfBirth;
    this.ticket = ticket;
  }

  public UserRegistrationDTO() {

  }

  public static Builder getBuilder(UserRegistration userReg) {
    return new Builder(userReg);
  }

  public static class Builder {

    private UserRegistrationDTO build;

    Builder(UserRegistration userRegistration) {
      build = new UserRegistrationDTO();
      build.setId(userRegistration.getId());
      build.setCity(userRegistration.getCity());
      build.setEmail(userRegistration.getEmail());
      build.setFirstName(userRegistration.getFirstName());
      build.setGender(userRegistration.getGender());
      build.setNickname(userRegistration.getNickname());
      build.setPhoneNumber(userRegistration.getPhoneNumber());
      build.setStreet(userRegistration.getStreet());
      build.setSurname(userRegistration.getSurname());
      build.setTicket(TicketDTO.getBuilder(userRegistration.getTicket()).build());
      build.setYearOfBirth(userRegistration.getYearOfBirth());
      build.setZipCode(userRegistration.getZipCode());
      build.setCreated(userRegistration.getCreated());
      build.setPaid(userRegistration.isPaid());
    }

    public UserRegistrationDTO build() {
      return build;
    }
  }

}

package cz.jo.zizcon.service;

import org.apache.poi.ss.usermodel.Workbook;

public interface ExporterService {

  Workbook exportActivities();

}

package cz.jo.zizcon.service.impl;

import cz.jo.zizcon.dao.ActivityRepository;
import cz.jo.zizcon.model.entity.Activity;
import cz.jo.zizcon.model.entity.Activity.Type;
import cz.jo.zizcon.model.entity.ActivityAttendee;
import cz.jo.zizcon.model.entity.ActivityAttendingLimit;
import cz.jo.zizcon.model.entity.User;
import cz.jo.zizcon.service.ExporterService;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
  
@Component
public class ExporterServiceImpl implements ExporterService {

  @Autowired
  ActivityRepository activityRepository;

  Map<String, XSSFCellStyle> styles;

  @Override
  public Workbook exportActivities() {
    Iterable<Activity> activityList = activityRepository.findAll();
    return createWorkBookWithActivities(activityList);
  }

  private Workbook createWorkBookWithActivities(Iterable<Activity> activityList) {
    XSSFWorkbook resultWorkbook = new XSSFWorkbook();

    createStyles(resultWorkbook);

    for (Activity activity : activityList) {
      createSeparateSheetForActivity(resultWorkbook, activity);
    }
    return resultWorkbook;
  }

  private void createStyles(XSSFWorkbook resultWorkbook) {

    this.styles = new HashMap<>();

    XSSFCellStyle  basicStyle = resultWorkbook.createCellStyle();
    basicStyle.setBorderBottom(CellStyle.BORDER_THIN);
    basicStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
    basicStyle.setBorderLeft(CellStyle.BORDER_THIN);
    basicStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
    basicStyle.setBorderRight(CellStyle.BORDER_THIN);
    basicStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
    basicStyle.setBorderTop(CellStyle.BORDER_THIN);
    basicStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

    styles.put("basic", basicStyle);

    XSSFCellStyle  titleText = resultWorkbook.createCellStyle();
    XSSFFont title = resultWorkbook.createFont();
    title.setFontHeightInPoints((short) 16);
    title.setBold(true);
    titleText.setFont(title);

    styles.put("title", titleText);



    XSSFCellStyle  boldText = resultWorkbook.createCellStyle();
    boldText.setBorderBottom(CellStyle.BORDER_THIN);
    boldText.setBottomBorderColor(IndexedColors.BLACK.getIndex());
    boldText.setBorderLeft(CellStyle.BORDER_THIN);
    boldText.setLeftBorderColor(IndexedColors.BLACK.getIndex());
    boldText.setBorderRight(CellStyle.BORDER_THIN);
    boldText.setRightBorderColor(IndexedColors.BLACK.getIndex());
    boldText.setBorderTop(CellStyle.BORDER_THIN);
    boldText.setTopBorderColor(IndexedColors.BLACK.getIndex());
    XSSFFont font = resultWorkbook.createFont();
    font.setFontHeightInPoints((short) 12);
    font.setBold(true);
    boldText.setFont(font);

    styles.put("boldText", boldText);

    XSSFCellStyle  smallText = resultWorkbook.createCellStyle();
    smallText.setBorderBottom(CellStyle.BORDER_THIN);
    smallText.setBottomBorderColor(IndexedColors.BLACK.getIndex());
    smallText.setBorderLeft(CellStyle.BORDER_THIN);
    smallText.setLeftBorderColor(IndexedColors.BLACK.getIndex());
    smallText.setBorderRight(CellStyle.BORDER_THIN);
    smallText.setRightBorderColor(IndexedColors.BLACK.getIndex());
    smallText.setBorderTop(CellStyle.BORDER_THIN);
    smallText.setTopBorderColor(IndexedColors.BLACK.getIndex());
    XSSFFont font2 = resultWorkbook.createFont();
    font2.setFontHeightInPoints((short)8);
    font2.setBold(false);
    font2.setItalic(true);
    smallText.setFont(font2);
    styles.put("smallText", smallText);

  }

  private void createSeparateSheetForActivity(XSSFWorkbook resultWorkbook, Activity activity) {
    String activityTitle = activity.getTextation().get(0).getTitle();
    XSSFSheet sheet = resultWorkbook.createSheet(WorkbookUtil.createSafeSheetName(activity.getStringId()));

    XSSFRow row = sheet.createRow((short) 0);
    XSSFCell cell = row.createCell(0);
    cell.setCellValue(activityTitle);
    cell.setCellStyle(styles.get("title"));
    sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 1));

    adActivityTime(1, sheet, activity);
    addActivityType(2, sheet, activity);
    addActivityOrg(3, sheet, activity);
    addActivityPlace(4, sheet, activity);
    addAttendees(5, sheet, activity);

    sheet.addMergedRegion(new CellRangeAddress(5, 5, 0, 1));

    sheet.setColumnWidth(0 ,11000);
    sheet.setColumnWidth(1, 11000);
  }

  private void addAttendees(int i, XSSFSheet sheet, Activity activity) {
    XSSFRow row = sheet.createRow(i);
    XSSFCell cellTitle = row.createCell(0);
    cellTitle.setCellValue("Účastníci");
    cellTitle.setCellStyle(styles.get("boldText"));
    int j = i + 1;
    for (ActivityAttendee activityAttendee : activity.getAttendees()) {
      Row attendeeRow = sheet.createRow(j);
      Cell attendeeName = attendeeRow.createCell(0);
      attendeeName.setCellStyle(styles.get("basic"));
      attendeeName.setCellValue(formatName(activityAttendee.getUser()));
      Cell attendeePhone = attendeeRow.createCell(1);
      attendeePhone.setCellValue(activityAttendee.getUser().getPhoneNumber());
      attendeePhone.setCellStyle(styles.get("basic"));
      j++;
    }

    if (activity.getLimits() == null || activity.getLimits().isEmpty()) {
    } else {
      Map<String, ActivityAttendingLimit> limits = new HashMap<>();
      for (ActivityAttendingLimit limit : activity.getLimits()) {
        limits.put(limit.getGender(), limit);
      }
      Map<String, Integer> countByGender = ProgramServiceHelper.countAttendeesByGender(activity.getAttendees());
      Map<String, Integer> freeSpots = ProgramServiceHelper.calculateFreeSpots(limits, countByGender);

      if (freeSpots.containsKey("F")) {
        j = addFreeSpots(sheet, j, "F", freeSpots.get("F"));
      }
      if (freeSpots.containsKey("M")) {
        j = addFreeSpots(sheet, j, "M", freeSpots.get("M"));
      }
      if (freeSpots.containsKey("A")) {
        j = addFreeSpots(sheet, j, "A", freeSpots.get("A"));
      }
    }
  }

  private int addFreeSpots(XSSFSheet sheet, int rowNum, String gender, Integer numOfSpots) {
    for (int i = 0; i < numOfSpots; i++) {
      XSSFRow emptySpace = sheet.createRow(rowNum);
      XSSFCell emptySpaceTitle = emptySpace.createCell(0);
      XSSFCell emptySpacephone = emptySpace.createCell(1);
      emptySpacephone.setCellStyle(styles.get("basic"));
      emptySpaceTitle.setCellStyle(styles.get("smallText"));
      switch (gender) {
        case "F":
          emptySpaceTitle.setCellValue("Volné (Žena)");
          break;
        case "M":
          emptySpaceTitle.setCellValue("Volné (Muž)");
          break;
        case "A":
          emptySpaceTitle.setCellValue("Volné");
          break;
        default:
          emptySpaceTitle.setCellValue("Volné");
          break;
      }
      rowNum++;
    }
    return rowNum;
  }

  private String formatName(User user) {
    return user.getFirstName() +
        " " +
        user.getSurname() +
        " (" +
        user.getEmail() +
        ")";
  }

  private void addActivityOrg(int i, XSSFSheet sheet, Activity activity) {
    XSSFRow row = sheet.createRow(i);
    XSSFCell cellTitle = row.createCell(0);
    cellTitle.setCellStyle(styles.get("boldText"));
    cellTitle.setCellValue("Uvaděč");
    XSSFCell cell = row.createCell(1);
    cell.setCellStyle(styles.get("basic"));
    cell.setCellValue(activity.getOrganizer());
  }

  private void addActivityPlace(int i, XSSFSheet sheet, Activity activity) {
    XSSFRow row = sheet.createRow(i);
    XSSFCell cellTitle = row.createCell(0);
    cellTitle.setCellStyle(styles.get("boldText"));
    cellTitle.setCellValue("Místo");
    XSSFCell cell = row.createCell(1);
    cell.setCellStyle(styles.get("basic"));
    cell.setCellValue("");
  }

  private void addActivityType(int i, XSSFSheet sheet, Activity activity) {
    XSSFRow row = sheet.createRow(i);
    XSSFCell cellTitle = row.createCell(0);
    cellTitle.setCellStyle(styles.get("boldText"));
    cellTitle.setCellValue("Typ hry");
    XSSFCell cell = row.createCell(1);
    cell.setCellStyle(styles.get("basic"));
    cell.setCellValue(translate(activity.getActivityType()));
  }

  private void adActivityTime(int i, XSSFSheet sheet, Activity activity) {
    XSSFRow row = sheet.createRow(i);
    XSSFCell cellTitle = row.createCell(0);
    cellTitle.setCellStyle(styles.get("boldText"));
    cellTitle.setCellValue("Čas");
    XSSFCell cell = row.createCell(1);
    cell.setCellStyle(styles.get("basic"));
    SimpleDateFormat fromFormat = new SimpleDateFormat("EEE HH:mm");
    SimpleDateFormat toFormat = new SimpleDateFormat("HH:mm");
    cell.setCellValue(fromFormat.format(activity.getFromDate()) + "-" + toFormat.format(activity.getToDate()));
  }

  private String translate(Type activityType) {
    String type = "";
    switch (activityType) {
      case RPG:
        type = "Rolová hra (RPG)";
        break;
      case LARP:
        type = "LARP";
        break;
      case BOARD_GAME:
        type = "Deskovka";
        break;
      case OTHER:
        type = "Doprovodný program";
        break;
    }
    return type;
  }


}

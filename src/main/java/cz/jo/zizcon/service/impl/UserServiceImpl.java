package cz.jo.zizcon.service.impl;

import cz.jo.zizcon.dao.UserRepository;
import cz.jo.zizcon.model.ZizconUserPrincipal;
import cz.jo.zizcon.model.dto.UserDTO;
import cz.jo.zizcon.model.entity.User;
import cz.jo.zizcon.service.UserService;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created by User on 20.6.2017.
 */
@Service
public class UserServiceImpl implements UserService {

  @Autowired
  private UserRepository userRepository;

  @Override
  @Transactional
  public UserDetails loadUserByUsername(String username) {
    User user = userRepository.findByUsername(username);
    if(user == null){
      throw new UsernameNotFoundException(String.format("User %s not found!", username));
    } else {
      return new ZizconUserPrincipal(user);
    }
  }

  @Override
  public UserDTO findUserByUserNameDto(String username) {
    return UserDTO.getBuilder(userRepository.findByUsername(username)).build();
  }

  @Override
  public Iterable<User> findAll() {
    return userRepository.findAll();
  }

}

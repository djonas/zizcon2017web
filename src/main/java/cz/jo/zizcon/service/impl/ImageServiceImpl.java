package cz.jo.zizcon.service.impl;

import cz.jo.zizcon.dao.ActivityRepository;
import cz.jo.zizcon.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * Created by User on 25.5.2017.
 */

@Service("imageService")
public class ImageServiceImpl implements ImageService {

  private final ActivityRepository activityRepository;

  @Autowired
  public ImageServiceImpl(ActivityRepository activityRepository) {
    this.activityRepository = activityRepository;
  }

  @Cacheable("images")
  @Override
  public byte[] getBigImageDataByID(String id) {
    return activityRepository.findByStringId(id).getImageBig();
  }

  @Cacheable("imagesThumb")
  @Override
  public byte[] getThumbImageDataByID(String id) {
    return activityRepository.findByStringId(id).getImageThumb();
  }
}

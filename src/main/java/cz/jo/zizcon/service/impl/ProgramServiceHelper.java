package cz.jo.zizcon.service.impl;

import cz.jo.zizcon.model.ZizconUserPrincipal;
import cz.jo.zizcon.model.dto.ActivityDTO.LimitDTO;
import cz.jo.zizcon.model.entity.Activity;
import cz.jo.zizcon.model.entity.ActivityAttendee;
import cz.jo.zizcon.model.entity.ActivityAttendingLimit;
import cz.jo.zizcon.model.entity.UserRegistration;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.joda.time.DateTime;

public class ProgramServiceHelper {

  private ProgramServiceHelper(){

  }

  public static final String GENDER_MALE = "M";
  public static final String GENDER_FEMALE = "F";
  public static final String GENDER_OTHER = "O";

  public static final String LIMIT_MALE = "M";
  public static final String LIMIT_FEMALE = "F";
  public static final String LIMIT_OTHER = "A";

  public static boolean validIfNotFull(Activity activity, ZizconUserPrincipal principal) {
    boolean valid;
    if(activity.getLimits() == null || activity.getLimits().isEmpty()){
      valid = true;
    } else {
      Map<String, ActivityAttendingLimit> limits = new HashMap<>();
      for(ActivityAttendingLimit limit : activity.getLimits()){
        limits.put(limit.getGender(),limit);
      }
      Map<String, Integer> countByGender = countAttendeesByGender(activity.getAttendees());
      String currentGender = principal.getUser().getGender();
      Map<String, Integer> freeSpots = calculateFreeSpots(limits,countByGender);
      //gender limits set
      if(limits.containsKey(currentGender) && !limits.containsKey(LIMIT_OTHER)){
        valid = freeSpots.get(currentGender) > 0;
        //both limits set
      } else if (limits.containsKey(currentGender) && limits.containsKey(LIMIT_OTHER)){
        valid = freeSpots.get(currentGender) > 0 || freeSpots.get(LIMIT_OTHER) > 0;
        //only ambiguous limits set
      } else if (!limits.containsKey(currentGender) && limits.containsKey(LIMIT_OTHER)){
        valid = freeSpots.get(LIMIT_OTHER) > 0;
      } else{
        valid = false;
      }
    }
    return valid;
  }

  public static Map<String,Integer> calculateFreeSpots(Map<String, ActivityAttendingLimit> limits, Map<String, Integer> countByGender) {
    Map<String, Integer> freeSpots = new HashMap<>();
    Integer femaleSpots = (limits.getOrDefault(LIMIT_FEMALE, new ActivityAttendingLimit()).getMax() - countByGender.getOrDefault(GENDER_FEMALE,0));
    freeSpots.put("F", femaleSpots);
    Integer maleSpots = (limits.getOrDefault(LIMIT_MALE, new ActivityAttendingLimit()).getMax() - countByGender.getOrDefault(GENDER_MALE,0));
    freeSpots.put("M", maleSpots);
    Integer otherSpots = (limits.getOrDefault(LIMIT_OTHER, new ActivityAttendingLimit()).getMax() - countByGender.getOrDefault(GENDER_OTHER,0));
    if(maleSpots < 0){
      otherSpots = otherSpots - Math.abs(maleSpots);
    }
    if(femaleSpots < 0){
      otherSpots = otherSpots - Math.abs(femaleSpots);
    }
    freeSpots.put("A",otherSpots);


    return freeSpots;
  }

  public static Map<String, Integer> countAttendeesByGender(Collection<ActivityAttendee> attendees) {
    Map<String, Integer> genderCount = new HashMap<>();
    if(attendees != null) {
      for (ActivityAttendee activityAttendee : attendees) {
        genderCount.merge(activityAttendee.getUser().getGender(), 1, Integer::sum);
      }
    }
    return genderCount;
  }

  public static boolean validIfInTicketRange(Activity activity, UserRegistration userRegistration) {
    if(userRegistration == null){
      return false;
    }
    DateTime activityStart = new DateTime(activity.getFromDate());
    DateTime ticketValidFrom = new DateTime(userRegistration.getTicket().getValidFrom());
    DateTime ticketValidTo = new DateTime(userRegistration.getTicket().getValidTo());
    return activityStart.isAfter(ticketValidFrom) && activityStart.isBefore(ticketValidTo);
  }

  public static boolean validIfNotAlreadyAttending(Activity activity, ZizconUserPrincipal principal) {
    return activity.getAttendees().stream().noneMatch(attendee -> attendee.getUser().getEmail().equals(principal.getUser().getEmail()));
  }


  public static Map<String,LimitDTO> createLimitDtos(Activity activity) {
    Map<String,LimitDTO> limitDTOMap = new HashMap<>();
    if(activity.getLimits() != null && !activity.getLimits().isEmpty()) {
      Map<String, ActivityAttendingLimit> limits = new HashMap<>();
      for (ActivityAttendingLimit limit : activity.getLimits()) {
        limits.put(limit.getGender(), limit);
      }
      Map<String, Integer> countByGender = countAttendeesByGender(activity.getAttendees());
      Map<String, Integer> freeSpots = calculateFreeSpots(limits, countByGender);

      for (String key : limits.keySet()) {
        if (limits.containsKey(key)) {
          LimitDTO limitDTO = new LimitDTO();
          limitDTO.setName(key);
          limitDTO.setMin(limits.get(key).getMin());
          limitDTO.setMax(limits.get(key).getMax());
          if(freeSpots.getOrDefault(key, 0) < 0){
            limitDTO.setUsed(limits.get(key).getMax());
          } else {
            limitDTO.setUsed(limits.get(key).getMax() - freeSpots.getOrDefault(key, 0));
          }
          limitDTOMap.put(key, limitDTO);
        }
      }

    }
    return limitDTOMap;
  }

  public static boolean validIfNotClosed() {
    DateTime now = new DateTime();
    DateTime closure = new DateTime().withYear(2017).withDayOfMonth(14).withMonthOfYear(9).withTimeAtStartOfDay();
    return now.isBefore(closure);
  }
}

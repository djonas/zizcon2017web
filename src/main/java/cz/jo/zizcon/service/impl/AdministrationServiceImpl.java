package cz.jo.zizcon.service.impl;

import cz.jo.zizcon.dao.ActivityRepository;
import cz.jo.zizcon.dao.RoleRepository;
import cz.jo.zizcon.dao.UserRegistrationRepository;
import cz.jo.zizcon.dao.UserRepository;
import cz.jo.zizcon.mailing.MailingService;
import cz.jo.zizcon.model.dto.AdminOverwievDTO;
import cz.jo.zizcon.model.dto.UserRegistrationDTO;
import cz.jo.zizcon.model.entity.Role;
import cz.jo.zizcon.model.entity.User;
import cz.jo.zizcon.model.entity.UserRegistration;
import cz.jo.zizcon.service.AdministrationService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.transaction.Transactional;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AdministrationServiceImpl implements AdministrationService {

  @Autowired
  ActivityRepository activityRepository;

  @Autowired
  UserRegistrationRepository userRegistrationRepository;

  @Autowired
  UserRepository userRepository;

  @Autowired
  private RoleRepository roleRepository;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Autowired
  private MailingService mailingService;


  @Override
  @Transactional
  public AdminOverwievDTO getOverwiev() {
    AdminOverwievDTO adminOverwievDTO = new AdminOverwievDTO();
    resolveRegistrations(adminOverwievDTO);
    resolveActivities(adminOverwievDTO);
    return adminOverwievDTO;
  }

  @Override
  @Transactional
  public List<UserRegistrationDTO> getRegistrations() {
    Iterable<UserRegistration> registrations = userRegistrationRepository.findAll();
    List<UserRegistrationDTO> registrationsDto = new ArrayList<>();
    registrations.forEach(registration -> registrationsDto.add(UserRegistrationDTO.getBuilder(registration).build()));
    return registrationsDto;
  }

  @Override
  @Transactional
  public void flagAsPaid(int id) {
    //find registration
    UserRegistration registration = userRegistrationRepository.findOne(id);
    if(!registration.isPaid()) {
      //set paid
      registration.setPaid(true);
      //store
      userRegistrationRepository.save(registration);
      //create user from registration
      User newUser = createUserFromRegistration(registration);
      //generate password
      String password = generatePassword();
      newUser.setPassword(passwordEncoder.encode(password));
      userRepository.save(newUser);
      //send email to user with his login access and that he paid
      mailingService.sendPaidEmails(UserRegistrationDTO.getBuilder(registration).build(), newUser.getUsername(), password);
    } else {
      throw new IllegalStateException("User registration already paid!");
    }
  }

  private User createUserFromRegistration(UserRegistration registration) {
    User newUser = new User();
    newUser.setUsername(registration.getEmail());
    newUser.setEmail(registration.getEmail());
    newUser.setSurname(registration.getSurname());
    newUser.setFirstName(registration.getFirstName());
    newUser.setRoles(setUserRoles());
    newUser.setEnabled(true);
    
    newUser.setCity(registration.getCity());
    newUser.setGender(registration.getGender());
    newUser.setPhoneNumber(registration.getPhoneNumber());
    newUser.setStreet(registration.getStreet());
    newUser.setZipCode(registration.getZipCode());
    newUser.setYearOfBirth(Integer.parseInt(registration.getYearOfBirth()));


    return newUser;
  }

  private String generatePassword() {
    //RandomStringGenerator generator = new RandomStringGenerator.Builder().withinRange('a','Z').build();
    //return generator.generate(10);
    return  RandomStringUtils.randomAlphanumeric(10);
  }

  private Collection<Role> setUserRoles() {
    Role userRole = roleRepository.findByName("ROLE_USER");
    List<Role> roles = new ArrayList<>();
    roles.add(userRole);
    return roles;
  }

  private void resolveRegistrations(AdminOverwievDTO adminOverwievDTO) {
    Iterable<UserRegistration> userRegistrations = userRegistrationRepository.findAll();
    int paid = 0;
    for (UserRegistration reg : userRegistrations) {
      if (reg.isPaid()) {
        paid++;
      }
    }
    adminOverwievDTO.setNumOfRegistered((int) userRegistrationRepository.count());
    adminOverwievDTO.setNumOfPaidRegistrations(paid);
  }

  private void resolveActivities(AdminOverwievDTO adminOverwievDTO) {
    adminOverwievDTO.setNumOfActivities((int) activityRepository.count());
  }


}

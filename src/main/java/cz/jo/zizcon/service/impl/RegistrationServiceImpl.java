package cz.jo.zizcon.service.impl;

import cz.jo.zizcon.dao.TicketRepository;
import cz.jo.zizcon.dao.UserRegistrationRepository;
import cz.jo.zizcon.exceptions.EmailAlreadyRegisteredException;
import cz.jo.zizcon.mailing.MailingService;
import cz.jo.zizcon.model.dto.TicketDTO;
import cz.jo.zizcon.model.dto.UserRegistrationDTO;
import cz.jo.zizcon.model.entity.Ticket;
import cz.jo.zizcon.model.entity.UserRegistration;
import cz.jo.zizcon.service.RegistrationService;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 3.6.2017.
 */
@Service
public class RegistrationServiceImpl implements RegistrationService {

  private static final Logger logger = LoggerFactory.getLogger(RegistrationServiceImpl.class);

  @Autowired
  UserRegistrationRepository userRegistrationRepository;

  @Autowired
  TicketRepository ticketRepository;

  @Autowired
  MailingService mailingService;

  @Override
  public UserRegistrationDTO getById(Integer id) {
    return null;
  }

  @Override
  public List<UserRegistrationDTO> getAll() {
    return null;
  }

  @Override
  public UserRegistrationDTO save(UserRegistrationDTO dto) {
    logger.debug(String.format("Saving new registration for %s %s, %s", dto.getFirstName(), dto.getSurname(), dto.getEmail()));
    UserRegistration regEntity = UserRegistration.getBuilder(dto).build();
    regEntity.setCreated(new Date());
    try {
      regEntity = userRegistrationRepository.save(regEntity);
    } catch (DataIntegrityViolationException e) {
      logger.error(String.format("Storage of the new registration would violate data integrity rules, store didn't happen"));
      if (emailAlreadyRegistered(regEntity.getEmail())) {
        logger.error(String.format("The email used in this registration (%s) is alreadz registered!", regEntity.getEmail()));
        throw new EmailAlreadyRegisteredException();
      }
    }
    logger.debug(String.format("Registration of %s %s, %s, was sucessfull", dto.getFirstName(), dto.getSurname(), dto.getEmail()));

    dto = UserRegistrationDTO.getBuilder(regEntity).build();
    //Send emails
    mailingService.sendRegistrationEmails(dto);
    return dto;
  }

  private boolean emailAlreadyRegistered(String email) {
    return userRegistrationRepository.findByEmail(email) != null;
  }

  @Override
  public UserRegistrationDTO update(UserRegistrationDTO entity) {
    return null;
  }

  @Override
  public void delete(UserRegistrationDTO entity) {

  }

  @Cacheable("allTickets")
  @Override
  public List<TicketDTO> getAllTicketTypes() {
    return prepareTicketList(ticketRepository.findByRegistrableFromBeforeAndRegistrableToAfter(new Date(), new Date()));
  }

  @Cacheable("tickets")
  @Override
  public TicketDTO getTicketByCode(String s) {
    return TicketDTO.getBuilder(ticketRepository.getByCode(s)).build();
  }

  private List<TicketDTO> prepareTicketList(Iterable<Ticket> all) {
    ArrayList<TicketDTO> dtos = new ArrayList<>();
    all.forEach(ticket -> dtos.add(TicketDTO.getBuilder(ticket).build()));
    return dtos;
  }
}

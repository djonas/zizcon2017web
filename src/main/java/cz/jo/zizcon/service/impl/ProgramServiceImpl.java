package cz.jo.zizcon.service.impl;

import cz.jo.zizcon.dao.ActivityAttendeeRepository;
import cz.jo.zizcon.dao.ActivityRepository;
import cz.jo.zizcon.dao.UserRegistrationRepository;
import cz.jo.zizcon.dao.UserRepository;
import cz.jo.zizcon.model.ZizconUserPrincipal;
import cz.jo.zizcon.model.dto.ActivityDTO;
import cz.jo.zizcon.model.dto.AttendanceChangeDTO;
import cz.jo.zizcon.model.entity.Activity;
import cz.jo.zizcon.model.entity.Activity.Type;
import cz.jo.zizcon.model.entity.ActivityAttendee;
import cz.jo.zizcon.model.entity.User;
import cz.jo.zizcon.model.entity.UserRegistration;
import cz.jo.zizcon.service.ProgramService;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * Created by User on 6.6.2017.
 */
@Service
public class ProgramServiceImpl implements ProgramService {

  private final ActivityRepository activityRepository;

  private final UserRegistrationRepository registrationRepository;

  private final UserRepository userRepository;

  private final ActivityAttendeeRepository activityAttendeeRepository;


  private Logger logger = LoggerFactory.getLogger(ProgramServiceImpl.class);

  @Autowired
  public ProgramServiceImpl(ActivityRepository activityRepository, UserRegistrationRepository registrationRepository, UserRepository userRepository,
      ActivityAttendeeRepository activityAttendeeRepository) {
    this.activityRepository = activityRepository;
    this.registrationRepository = registrationRepository;
    this.userRepository = userRepository;
    this.activityAttendeeRepository = activityAttendeeRepository;
  }

  @Override
  @Transactional
  public Page<ActivityDTO> getAll(Pageable pageable, Locale locale, boolean myActivities) {
    Page<Activity> activities;
    ZizconUserPrincipal principal = getCurrentUserPrincipal();
    if(myActivities && principal != null){
      activities = activityRepository.findByAttendees_User_Username(principal.getUsername(), pageable);
    } else {
      activities = activityRepository.findAll(pageable);
    }

    return activities.map(getConverter());
  }

  @Override
  @Transactional
  public Page<ActivityDTO> getForType(String type, Locale locale, boolean myActivities, Pageable pageable) {
    Page<Activity> activities;
    ZizconUserPrincipal principal = getCurrentUserPrincipal();
    if(myActivities && principal != null){
      activities = activityRepository.findByActivityTypeAndAttendees_User_Username(Type.getByName(type), principal.getUsername(), pageable);
    } else {
      activities = activityRepository.findByActivityType(Type.getByName(type), pageable);
    }
    return activities.map(getConverter());
  }

  @Override
  public Optional<ActivityDTO> getDetail(String id, Locale locale) {
    return Optional.ofNullable(getConverter().convert(activityRepository.findByStringId(id)));
  }

  @Override
  @Transactional
  public AttendanceChangeDTO attend(String activityId) {
    AttendanceChangeDTO attendanceChangeDTO = new AttendanceChangeDTO();
    try{
      Activity activity = activityRepository.findByStringId(activityId);
      ZizconUserPrincipal principalZ = getCurrentUserPrincipal();
      assert principalZ != null;
      User user = userRepository.findByUsername(principalZ.getUser().getUsername());
      String validationError = validate(activity,true);
      if(StringUtils.isEmpty(validationError)){
        ActivityAttendee activityAttendee = new ActivityAttendee();
        activityAttendee.setUser(user);
        activityAttendee.setActivity(activity);
        activityAttendee.setTimeAttended(new Date());
        user.getUserActivities().add(activityAttendee);
        activity.getAttendees().add(activityAttendee);
        activityAttendeeRepository.save(activityAttendee);
        activityRepository.save(activity);
        attendanceChangeDTO.setCompleted(true);
        attendanceChangeDTO.setMessage("KOK");
      } else {
        attendanceChangeDTO.setCompleted(false);
        attendanceChangeDTO.setMessage(validationError);
      }
    } catch (Exception e){
      logger.error(e.getMessage());
      attendanceChangeDTO.setCompleted(false);
      attendanceChangeDTO.setMessage(e.getMessage());
    }
    return attendanceChangeDTO;
  }

  @Override
  @Transactional
  public AttendanceChangeDTO cancelAttendance(String activityId) {
    AttendanceChangeDTO attendanceChangeDTO = new AttendanceChangeDTO();
    try{
      Activity activity = activityRepository.findByStringId(activityId);
      ZizconUserPrincipal principalZ = getCurrentUserPrincipal();
      assert principalZ != null;
      User user = userRepository.findByUsername(principalZ.getUser().getUsername());
      Optional<ActivityAttendee> activityAttendee = activity.getAttendees().stream()
          .filter(attendee -> attendee.getUser().getUsername().equals(user.getUsername()))
          .findFirst();
      if(activityAttendee.isPresent()){

        activity.getAttendees().removeIf(activityAtt -> activityAtt.equals(activityAttendee.get()));
        user.getUserActivities().removeIf(userAtt -> userAtt.equals(activityAttendee.get()));
        activityAttendeeRepository.delete(activityAttendee.get());
        activityRepository.save(activity);
        userRepository.save(user);

        attendanceChangeDTO.setCompleted(true);
        attendanceChangeDTO.setMessage("OK");
      } else {
        attendanceChangeDTO.setCompleted(false);
        attendanceChangeDTO.setMessage("activity.notAttendable.notAttending");
      }
    } catch (Exception e){
      attendanceChangeDTO.setCompleted(false);
      attendanceChangeDTO.setMessage(e.getMessage());
    }
    return attendanceChangeDTO;
  }


  private Converter<Activity, ActivityDTO> getConverter() {
    return this::convertAndValidate;
  }

  private ActivityDTO convertAndValidate(Activity activity) {
    ActivityDTO dto = ActivityDTO.getBuilder(activity).build();
    String validationResult = validate(activity, false);
    dto.setReasonNotAttendable(validationResult);
    dto.setAttendable(StringUtils.isEmpty(validationResult));
    return dto;
  }

  private String validate(Activity activity, boolean forceUserLogged) {
    ZizconUserPrincipal principalZ = getCurrentUserPrincipal();
    String validationResult = "";
    if (principalZ != null) {
      UserRegistration userRegistration = registrationRepository.findByEmail(principalZ.getUser().getEmail());
      if (!ProgramServiceHelper.validIfNotClosed()){
        validationResult = "activity.notAttendable.attendanceClosed";
      } else if (!ProgramServiceHelper.validIfNotAlreadyAttending(activity, principalZ)) {
        validationResult = "activity.notAttendable.alreadyAttending";
      } else if (!ProgramServiceHelper.validIfInTicketRange(activity, userRegistration)) {
        validationResult = "activity.notAttendable.outOfTicketRange";
      } else if (!ProgramServiceHelper.validIfNotFull(activity, principalZ)) {
        validationResult = "activity.notAttendable.activityFull";
      }
    } else if(forceUserLogged) {
      validationResult = "activity.notAttendable.mustLogIn";
    }
    return validationResult;
  }


  private ZizconUserPrincipal getCurrentUserPrincipal(){
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (!(auth instanceof AnonymousAuthenticationToken)) {
      return (ZizconUserPrincipal) auth.getPrincipal();
    }
    else {
      return null;
    }
  }

}

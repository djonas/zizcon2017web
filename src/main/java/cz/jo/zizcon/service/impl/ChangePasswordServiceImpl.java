package cz.jo.zizcon.service.impl;

import cz.jo.zizcon.dao.UserRepository;
import cz.jo.zizcon.exceptions.PasswordNotCorrectException;
import cz.jo.zizcon.mailing.MailingService;
import cz.jo.zizcon.model.dto.UserDTO;
import cz.jo.zizcon.model.entity.User;
import cz.jo.zizcon.service.ChangePasswordService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class ChangePasswordServiceImpl implements ChangePasswordService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Autowired
  MailingService mailingService;

  @Override
  public UserDTO changePassword(UserDTO userDto) {
    if(userDto.getPassword().equals(userDto.getPasswordAgain())){
      User user = userRepository.findByUsername(userDto.getUsername());
      user.setPassword(passwordEncoder.encode(userDto.getPassword()));
      userRepository.save(user);
      userDto.setPassword(null);
      userDto.setPasswordAgain(null);
      return userDto;
    } else {
      throw new PasswordNotCorrectException();
    }
  }

  @Override
  public void resetPassword(Long id) {
    User user = userRepository.findOne(id);
    String password = RandomStringUtils.randomAlphanumeric(10);
    user.setPassword(passwordEncoder.encode(password));
    mailingService.sendResetPasswordEmail(user,password);
    userRepository.save(user);
  }

}

package cz.jo.zizcon.service.impl;

import cz.jo.zizcon.dao.NewsRepository;
import cz.jo.zizcon.model.dto.NewsDTO;
import cz.jo.zizcon.model.entity.News;
import cz.jo.zizcon.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

/**
 * Created by User on 5.6.2017.
 */
@Service("newsService")
public class NewsServiceImpl implements NewsService {

  @Autowired
  NewsRepository newsRepository;


  @Override
  public List<NewsDTO> getNewsForFooter(Locale locale) {
    PageRequest pr = new PageRequest(0, 4);
    Page<News> news = newsRepository.findByLanguageOrderByPublishedDesc(locale.getLanguage(), pr);
    Page<NewsDTO> newsDtos = news.map(getConverter());
    return newsDtos.getContent();
  }

  @Override
  public Page<NewsDTO> getAll(Locale locale, Pageable pageable) {
    Page<News> news = newsRepository.findByLanguageOrderByPublishedDesc(locale.getLanguage(), pageable);
    Page<NewsDTO> newsDtos = news.map(getConverter());
    return newsDtos;
  }

  private Converter<News, NewsDTO> getConverter() {
    return new Converter<News, NewsDTO>() {
      @Override
      public NewsDTO convert(News news) {
        return NewsDTO.getBuilder(news).build();
      }
    };
  }

  @Cacheable("newsDetail")
  @Override
  public Optional<NewsDTO> getById(int id) {
    return Optional.ofNullable(NewsDTO.getBuilder(newsRepository.findOne(id)).build());
  }


}

package cz.jo.zizcon.service;

import cz.jo.zizcon.model.dto.UserDTO;
import cz.jo.zizcon.model.entity.User;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Created by User on 20.6.2017.
 */
public interface UserService extends UserDetailsService {

  UserDTO findUserByUserNameDto(String username);
  Iterable<User> findAll();

}

package cz.jo.zizcon.service;

import cz.jo.zizcon.exceptions.EmailAlreadyRegisteredException;

import java.io.Serializable;
import java.util.List;

/**
 * Generic interface for services that implements CRUD methods
 * <p/>
 * Created by Djonas
 */
interface CRUD<E, PK extends Serializable> {

  /**
   * Return entity, mostly by ID
   *
   * @param id unique value for entity
   * @return DTO object of Entity
   */
  E getById(PK id);

  /**
   * Find all entities from Repository
   *
   * @return List of DTO objects of entities
   */
  List<E> getAll();

  /**
   * Save new entity into Repository
   *
   * @param entity entity
   * @return DTO object
   */
  E save(E entity);

  /**
   * Update entity from Repository
   *
   * @param entity entity
   * @return DTO object
   */
  E update(E entity);

  /**
   * Delete entity from Repository
   *
   * @param entity entity
   */
  void delete(E entity);
}
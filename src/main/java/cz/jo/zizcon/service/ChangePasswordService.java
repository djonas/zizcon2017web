package cz.jo.zizcon.service;

import cz.jo.zizcon.model.dto.UserDTO;

public interface ChangePasswordService {
  UserDTO changePassword(UserDTO userDto);
  void resetPassword(Long id);
}

package cz.jo.zizcon.service;

import cz.jo.zizcon.model.dto.NewsDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


import java.util.List;
import java.util.Locale;
import java.util.Optional;

/**
 * Created by User on 5.6.2017.
 */
public interface NewsService {

  List<NewsDTO> getNewsForFooter(Locale locale);

  Page<NewsDTO> getAll(Locale locale, Pageable pageable);

  Optional<NewsDTO> getById(int id);
}

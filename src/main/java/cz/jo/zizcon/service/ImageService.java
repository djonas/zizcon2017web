package cz.jo.zizcon.service;

/**
 * Created by User on 25.5.2017.
 */
public interface ImageService {

  byte[] getBigImageDataByID(String id);

  byte[] getThumbImageDataByID(String id);
}

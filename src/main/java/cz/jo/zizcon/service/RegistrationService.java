package cz.jo.zizcon.service;

import cz.jo.zizcon.model.dto.TicketDTO;
import cz.jo.zizcon.model.dto.UserRegistrationDTO;

import java.util.List;

/**
 * Created by User on 3.6.2017.
 */
public interface RegistrationService extends CRUD<UserRegistrationDTO, Integer> {

  List<TicketDTO> getAllTicketTypes();

  TicketDTO getTicketByCode(String s);
}

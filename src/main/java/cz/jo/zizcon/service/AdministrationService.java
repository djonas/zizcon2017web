package cz.jo.zizcon.service;

import cz.jo.zizcon.model.dto.AdminOverwievDTO;
import cz.jo.zizcon.model.dto.UserRegistrationDTO;
import java.util.List;

public interface AdministrationService {

  AdminOverwievDTO getOverwiev();

  List<UserRegistrationDTO> getRegistrations();

  void flagAsPaid(int id);
}

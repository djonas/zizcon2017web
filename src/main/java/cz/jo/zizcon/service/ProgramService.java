package cz.jo.zizcon.service;

import cz.jo.zizcon.model.dto.ActivityDTO;
import cz.jo.zizcon.model.dto.AttendanceChangeDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


import java.util.Locale;
import java.util.Optional;

/**
 * Created by User on 6.6.2017.
 */
public interface ProgramService {

  Page<ActivityDTO> getAll(Pageable pageable, Locale locale, boolean myActivities);

  Page<ActivityDTO> getForType(String type, Locale locale, boolean myActivities, Pageable pageable);

  Optional<ActivityDTO> getDetail(String id, Locale locale);

  AttendanceChangeDTO attend(String activityId);
  AttendanceChangeDTO cancelAttendance(String activityId);
}

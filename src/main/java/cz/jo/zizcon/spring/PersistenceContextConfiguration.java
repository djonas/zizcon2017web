package cz.jo.zizcon.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by UserRegistration on 25.5.2017.
 */
@Configuration
@EnableCaching
@EnableJpaRepositories(basePackages = "cz.jo.zizcon.dao")
public class PersistenceContextConfiguration {

  @Autowired
  Environment env;

    /*
    @Value("#{mongo.host}")
    private String host;

    @Value("#{mongo.database}")
    private String database;

    @Bean
    public MongoDbFactory mongoDbFactory() throws Exception {
        return new SimpleMongoDbFactory(new MongoClient(host), database);
    }

    @Bean
    public MongoTemplate mongoTemplate() throws Exception {
        return new MongoTemplate(mongoDbFactory());

    }*/
}

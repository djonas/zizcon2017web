package cz.jo.zizcon.spring;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

public class ZizconSimpleUrlAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
  private RedirectStrategy redirectStrategy = new NoContextPathRedirectStrategy();
  private Logger logger = LoggerFactory.getLogger(ZizconSimpleUrlAuthenticationSuccessHandler.class);
  @Override
  public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
    logger.debug("redirecting auth succes");
    redirectStrategy.sendRedirect(httpServletRequest, httpServletResponse, "/");
  }


}

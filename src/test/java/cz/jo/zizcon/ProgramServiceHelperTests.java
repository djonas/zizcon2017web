package cz.jo.zizcon;

import cz.jo.zizcon.model.ZizconUserPrincipal;
import cz.jo.zizcon.model.entity.Activity;
import cz.jo.zizcon.model.entity.ActivityAttendee;
import cz.jo.zizcon.model.entity.ActivityAttendingLimit;
import cz.jo.zizcon.model.entity.Ticket;
import cz.jo.zizcon.model.entity.User;
import cz.jo.zizcon.model.entity.UserRegistration;
import cz.jo.zizcon.service.impl.ProgramServiceHelper;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProgramServiceHelperTests {

  @Test
  public void validRange() {
    Activity activity = new Activity();
    activity.setFromDate(new Date());
    activity.setToDate(new Date());

    UserRegistration userRegistration = new UserRegistration();
    Ticket ticket = new Ticket();
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH)-1);
    ticket.setValidFrom(cal.getTime());
    cal = Calendar.getInstance();
    cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH)+2);
    ticket.setValidTo(cal.getTime());
    userRegistration.setTicket(ticket);

    Assert.assertTrue("Must be true",ProgramServiceHelper.validIfInTicketRange(activity, userRegistration));
  }

  @Test
  public void validAttending(){

  }

  /**
   * Should be valid all the time
   */
  @Test
  public void  validLimitsNoLimits(){
    Activity activity = new Activity();
    Assert.assertTrue("Must be true",ProgramServiceHelper.validIfNotFull(activity, getPrincipal("F")));
    Assert.assertTrue("Must be true",ProgramServiceHelper.validIfNotFull(activity, getPrincipal("M")));
    Assert.assertTrue("Must be true",ProgramServiceHelper.validIfNotFull(activity, getPrincipal("O")));
  }

  /**
   * Only gender limits > therefore only participant of that genre can validly attend
   */
  @Test
  public void validLimitsOnlyGenderUnderLimit(){
    Activity activity = new Activity();
    activity.setLimits(createLimitsList(5,"F", "M"));
    activity.setAttendees(createAttendees(4,"F","M"));

    Assert.assertTrue("Must be true",ProgramServiceHelper.validIfNotFull(activity, getPrincipal("F")));
    Assert.assertTrue("Must be true",ProgramServiceHelper.validIfNotFull(activity, getPrincipal("M")));
    Assert.assertFalse("Must be false",ProgramServiceHelper.validIfNotFull(activity, getPrincipal("O")));
  }

  @Test
  public void validLimitsOnlyGenderOverLimit(){
    Activity activity = new Activity();
    activity.setLimits(createLimitsList(5,"F", "M"));
    activity.setAttendees(createAttendees(5,"F","M"));

    Assert.assertFalse("Must be false",ProgramServiceHelper.validIfNotFull(activity, getPrincipal("F")));
    Assert.assertFalse("Must be false",ProgramServiceHelper.validIfNotFull(activity, getPrincipal("M")));
    Assert.assertFalse("Must be false",ProgramServiceHelper.validIfNotFull(activity, getPrincipal("O")));
  }


  /**
   * Only other limits > therefore all can attend, given it is free.
   */
  @Test
  public void  validLimitsOnlyOtherLimitsLimits(){
    Activity activity = new Activity();
    activity.setLimits(createLimitsList(5,"A"));
    Assert.assertTrue("Must be true",ProgramServiceHelper.validIfNotFull(activity, getPrincipal("F")));
    Assert.assertTrue("Must be true",ProgramServiceHelper.validIfNotFull(activity, getPrincipal("M")));
    Assert.assertTrue("Must be true",ProgramServiceHelper.validIfNotFull(activity, getPrincipal("O")));
  }

  @Test
  public void validLimitsOnlyOtherLimitsUnderLimit(){
    Activity activity = new Activity();
    activity.setLimits(createLimitsList(8,"A"));
    activity.setAttendees(createAttendees(2,"F","M","O"));
    Assert.assertTrue("Must be true",ProgramServiceHelper.validIfNotFull(activity, getPrincipal("F")));
    Assert.assertTrue("Must be true",ProgramServiceHelper.validIfNotFull(activity, getPrincipal("M")));
    Assert.assertTrue("Must be true",ProgramServiceHelper.validIfNotFull(activity, getPrincipal("O")));
  }

  @Test
  public void validLimitsOnlyOtherLimitsOverLimit(){
    Activity activity = new Activity();
    activity.setLimits(createLimitsList(6,"A"));
    activity.setAttendees(createAttendees(2,"F","M","O"));
    Assert.assertFalse("Must be false",ProgramServiceHelper.validIfNotFull(activity, getPrincipal("F")));
    Assert.assertFalse("Must be false",ProgramServiceHelper.validIfNotFull(activity, getPrincipal("M")));
    Assert.assertFalse("Must be false",ProgramServiceHelper.validIfNotFull(activity, getPrincipal("O")));
  }

  /**
   * All limits > all can participate if there is free space
   */
  @Test
  public void  validLimitsAllLimits(){
    Activity activity = new Activity();
    activity.setLimits(createLimitsList(5,"A","F","M"));
    Assert.assertTrue("Must be true",ProgramServiceHelper.validIfNotFull(activity, getPrincipal("F")));
    Assert.assertTrue("Must be true",ProgramServiceHelper.validIfNotFull(activity, getPrincipal("M")));
    Assert.assertTrue("Must be true",ProgramServiceHelper.validIfNotFull(activity, getPrincipal("O")));
  }


  @Test
  public void  validLimitsAllLimitsGenderFullButOthersFree(){
    Activity activity = new Activity();
    activity.setLimits(createLimitsList(5,"A","F"));
    activity.setAttendees(createAttendees(5,"F"));

    Assert.assertTrue("Must be true",ProgramServiceHelper.validIfNotFull(activity, getPrincipal("F")));
    Assert.assertTrue("Must be true",ProgramServiceHelper.validIfNotFull(activity, getPrincipal("M")));
    Assert.assertTrue("Must be true",ProgramServiceHelper.validIfNotFull(activity, getPrincipal("O")));

    Activity activity2 = new Activity();
    activity2.setLimits(createLimitsList(5,"A","M"));
    activity2.setAttendees(createAttendees(5,"M"));

    Assert.assertTrue("Must be true",ProgramServiceHelper.validIfNotFull(activity2, getPrincipal("F")));
    Assert.assertTrue("Must be true",ProgramServiceHelper.validIfNotFull(activity2, getPrincipal("M")));
    Assert.assertTrue("Must be true",ProgramServiceHelper.validIfNotFull(activity2, getPrincipal("O")));

    Activity activity3 = new Activity();
    activity3.setLimits(createLimitsList(5,"A","M"));
    activity3.setAttendees(createAttendees(5,"O"));

    Assert.assertFalse("Must be false",ProgramServiceHelper.validIfNotFull(activity3, getPrincipal("F")));
    Assert.assertTrue("Must be true",ProgramServiceHelper.validIfNotFull(activity3, getPrincipal("M")));
    Assert.assertFalse("Must be false",ProgramServiceHelper.validIfNotFull(activity3, getPrincipal("O")));

  }



  private List<ActivityAttendingLimit> createLimitsList(int max, String... types){
    List<ActivityAttendingLimit> limits = new ArrayList<>();
    for(String type :types){
      ActivityAttendingLimit limit = new ActivityAttendingLimit();
      limit.setMax(max);
      limit.setMin(0);
      limit.setGender(type);
      limits.add(limit);
    }
    return limits;
  }

  private ZizconUserPrincipal getPrincipal(String gender){
    User user = new User();
    user.setGender(gender);
    ZizconUserPrincipal principal = new ZizconUserPrincipal(user);
    return principal;
  }

  private Set<ActivityAttendee> createAttendees(int count, String... genders) {
    Set<ActivityAttendee> attendees = new HashSet<>();
    for(String gender: genders){
      for(int i = 0 ; i < count ; i++){
        ActivityAttendee activityAttendee = new ActivityAttendee();
        User u = new User();
        u.setGender(gender);
        activityAttendee.setUser(u);
        attendees.add(activityAttendee);
      }
    }
    return attendees;
  }

}
